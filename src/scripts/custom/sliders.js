$(document).ready(function () {

    // block_header-slider

    $('.block_header-slider .slider').each(function (index) {
        $(this).slick({
            centerMode: false,
            rows: 0,
            speed: 400,
            cssEase: 'ease-out',
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipeToSlide: true,
            touchMove: true,
            touchThreshold: 100,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });

    // block_slider

    var showAmount = 0;
    if (Foundation.MediaQuery.atLeast('large')) {
        showAmount = 3;
    } else if (Foundation.MediaQuery.atLeast('medium')) {
        showAmount = 2;
    } else {
        showAmount = 1;
    }

    $('.block_slider .slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var calc = ((nextSlide) / (slick.slideCount - showAmount)) * 100;
        $(this).parents('section').find('.progress').css('background-size', calc + '% 100%').attr('aria-valuenow', calc);
    });

    $('.block_slider .slider').each(function (index) {
        $(this).slick({
            centerMode: false,
            rows: 0,
            speed: 150,
            arrows: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            swipeToSlide: true,
            touchMove: true,
            touchThreshold: 100,
            infinite: false,
            appendArrows: $(this).parents('section').find('.slider-buttons'),
            responsive: [
                {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                    }
                }
            ]
        });
    });

});