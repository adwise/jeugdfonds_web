if (document.querySelector('#fashion-form')) {
    document.addEventListener("DOMContentLoaded", function () {
        const phoneNumberWarning = document.getElementById("phoneNumberWarning");
        const postalCodeWarning = document.getElementById("postalCodeWarning");
        const sendButton = document.getElementById("sendButton");
        const dropdown = document.getElementById("kinderen");

        const kind1 = document.getElementById("kind1");
        const kind2 = document.getElementById("kind2");
        const kind3 = document.getElementById("kind3");
        const kind4 = document.getElementById("kind4");

        function phoneNumberCheck(phoneNumber) {
            const phoneNumberPattern = /^([0-9]){10,}$/;

            if (!phoneNumberPattern.test(phoneNumber)) {
                phoneNumberWarning.classList.remove("hidden");
                sendButton.disabled = true;
            } else {
                phoneNumberWarning.classList.add("hidden");
                sendButton.disabled = false;
            }
        }

        function postalCodeCheck(postalCode) {
            const postalCodePattern = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;

            if (!postalCodePattern.test(postalCode)) {
                postalCodeWarning.classList.remove("hidden");
                sendButton.disabled = true;
            } else {
                postalCodeWarning.classList.add("hidden");
                sendButton.disabled = false;
            }
        }

        const selectElement1 = document.getElementById("geslacht1");
        const selectElement2 = document.getElementById("geslacht2");
        const selectElement3 = document.getElementById("geslacht3");
        const selectElement4 = document.getElementById("geslacht4");

        const textareaElement1 = document.getElementById("andere-geslacht-textarea1");
        const textareaElement2 = document.getElementById("andere-geslacht-textarea2");
        const textareaElement3 = document.getElementById("andere-geslacht-textarea3");
        const textareaElement4 = document.getElementById("andere-geslacht-textarea4");

        selectElement1.addEventListener("change", function () {
            if (this.value === "Anders gekozen kind 1") {
                textareaElement1.style.display = "block";
            } else {
                textareaElement1.style.display = "none";
            }
        });

        selectElement2.addEventListener("change", function () {
            if (this.value === "Anders gekozen kind 2") {
                textareaElement2.style.display = "block";
            } else {
                textareaElement2.style.display = "none";
            }
        });

        selectElement3.addEventListener("change", function () {
            if (this.value === "Anders gekozen kind 3") {
                textareaElement3.style.display = "block";
            } else {
                textareaElement3.style.display = "none";
            }
        });

        selectElement4.addEventListener("change", function () {
            if (this.value === "Anders gekozen kind 4") {
                textareaElement4.style.display = "block";
            } else {
                textareaElement4.style.display = "none";
            }
        });

        function dropDownMenu() {
            let value = dropdown.value;

            switch (value) {
                case '1':
                    kind1.classList.remove("hidden");
                    kind2.classList.add("hidden");
                    kind3.classList.add("hidden");
                    kind4.classList.add("hidden");
                    break;
                case '2':
                    kind1.classList.remove("hidden");
                    kind2.classList.remove("hidden");
                    kind3.classList.add("hidden");
                    kind4.classList.add("hidden");
                    break;
                case '3':
                    kind1.classList.remove("hidden");
                    kind2.classList.remove("hidden");
                    kind3.classList.remove("hidden");
                    kind4.classList.add("hidden");
                    break;
                case '4':
                    kind1.classList.remove("hidden");
                    kind2.classList.remove("hidden");
                    kind3.classList.remove("hidden");
                    kind4.classList.remove("hidden");
                    break;
                default:
                    kind1.classList.add("hidden");
                    kind2.classList.add("hidden");
                    kind3.classList.add("hidden");
                    kind4.classList.add("hidden");
                    break;
            }
        }
        dropdown.addEventListener("change", dropDownMenu);
    });
}