$(document).ready(function() {

    $(document).foundation();

    // Mobile menu

    $('.nav__mobile-hamburger, .mobile__background, .mobile__menu-close').on('click', function () {
        $('body').toggleClass('navigating');
    });

    $('.click-submenu').on('click', function () {
        var parent = $(this).parent('.open-submenu');
        var submenu = parent.find('.mobile-menu--list---sub');
        var activeTwo = $(this).parent('li').find('a');
        var activeThree = $(this).parent('li').find('li.level-3.active a');
        parent.toggleClass('active-submenu');

        if (parent.hasClass('active-submenu')) {
            $(this).css('transform', 'rotate(270deg)');
            submenu.css('display', 'block');
            if (activeThree.length) {
                activeTwo.css('color', '#fefefe');
                activeThree.css('color', 'gold');
            }
        } else {
            $(this).css('transform', 'rotate(90deg)');
            submenu.css('display', 'none');
            if (activeThree.length) {
                activeTwo.css('color', 'gold');
            }
        }
    });

    var activeTwo = $('.level-2.active');
    if (activeTwo.length) {
        $('.level-1.active > a').css('color', '#0e131f');
    }

    // Matchheights

    $('.block_slider .wrapper h2').matchHeight();
    $('footer h2').matchHeight();
    $('.block_overview h3').matchHeight();

    // Cookie consent

    if (Cookies.get('jeugdMarketingAllowed') !== 'yes') {
        $('#cookie-consent').removeClass('hide');
        $('#cookie-consent .button').on({
            click: function() {
                Cookies.set('jeugdMarketingAllowed', 'yes', { expires: 365 });
                $('#cookie-consent').fadeOut(function() {
                    $(this).remove();
                });
            }
        });
    } else {
        $('#cookie-consent').remove();
    }

});

