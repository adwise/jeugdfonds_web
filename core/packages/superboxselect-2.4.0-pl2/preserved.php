<?php return array (
  'fbf0c4cc59d00dee59f7505384971520' => 
  array (
    'criteria' => 
    array (
      'name' => 'superboxselect',
    ),
    'object' => 
    array (
      'name' => 'superboxselect',
      'path' => '{core_path}components/superboxselect/',
      'assets_path' => '{assets_path}components/superboxselect/',
    ),
  ),
  '3eae9ba681479e6c8c6767aff33c0fcd' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.debug',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.debug',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'ed18e536ac6e10e2f3471fbc7efc9b36' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.advanced',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.advanced',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '49190cbefabd2095d7366e74d628fb24' => 
  array (
    'criteria' => 
    array (
      'category' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 47,
      'parent' => 0,
      'category' => 'SuperBoxSelect',
      'rank' => 0,
    ),
  ),
  'a60f014c9dd44d3017872079121780da' => 
  array (
    'criteria' => 
    array (
      'name' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 19,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'SuperBoxSelect',
      'description' => 'SuperBoxSelect runtime hooks - registers custom TV input types and includes javascripts on document edit pages.',
      'editor_type' => 0,
      'category' => 47,
      'cache_type' => 0,
      'plugincode' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
    ),
  ),
  '4206908050ed3f6a8ab022ac5efe17c7' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '25ac3b134c25ded39a7f8d68b79fd70d' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '9ac3fdb8bc9e4c648a0b9934e6bebfe6' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '6ae7237be7a683d5400d735111234611' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);