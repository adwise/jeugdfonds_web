<?php return array (
  'e4cfeb52e6e6d33c9628833392f87a64' => 
  array (
    'criteria' => 
    array (
      'name' => 'superboxselect',
    ),
    'object' => 
    array (
      'name' => 'superboxselect',
      'path' => '{core_path}components/superboxselect/',
      'assets_path' => '{assets_path}components/superboxselect/',
    ),
  ),
  '3ffe855d9a14ed68e60c90ff715de72d' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.debug',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.debug',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  'f3e4a7f160e4a022c56cb584d3f45632' => 
  array (
    'criteria' => 
    array (
      'key' => 'superboxselect.advanced',
    ),
    'object' => 
    array (
      'key' => 'superboxselect.advanced',
      'value' => '0',
      'xtype' => 'combo-boolean',
      'namespace' => 'superboxselect',
      'area' => 'system',
      'editedon' => '0000-00-00 00:00:00',
    ),
  ),
  '32ca806320d726329c985e3d6a0bec7e' => 
  array (
    'criteria' => 
    array (
      'category' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 47,
      'parent' => 0,
      'category' => 'SuperBoxSelect',
      'rank' => 0,
    ),
  ),
  'e28f11b0489e8b3e019e19d40d2dd382' => 
  array (
    'criteria' => 
    array (
      'name' => 'SuperBoxSelect',
    ),
    'object' => 
    array (
      'id' => 19,
      'source' => 0,
      'property_preprocess' => 0,
      'name' => 'SuperBoxSelect',
      'description' => 'SuperBoxSelect runtime hooks - registers custom TV input types and includes javascripts on document edit pages.',
      'editor_type' => 0,
      'category' => 47,
      'cache_type' => 0,
      'plugincode' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
      'locked' => 0,
      'properties' => 'a:0:{}',
      'disabled' => 0,
      'moduleguid' => '',
      'static' => 0,
      'static_file' => '',
      'content' => '/**
 * SuperBoxSelect Runtime Hooks
 *
 * Registers custom TV input types and includes javascripts on
 * document edit pages so that the TV can be used from within other extras
 * (i.e. MIGX, Collections)
 *
 * @package superboxselect
 * @subpackage plugin
 *
 * @event OnManagerPageBeforeRender
 * @event OnTVInputRenderList
 * @event OnTVInputPropertiesList
 * @event OnDocFormRender
 *
 * @var modX $modx
 */

$corePath = $modx->getOption(\'superboxselect.core_path\', null, $modx->getOption(\'core_path\') . \'components/superboxselect/\');
/** @var SuperBoxSelect $superboxselect */
$superboxselect = $modx->getService(\'superboxselect\', \'SuperBoxSelect\', $corePath . \'model/superboxselect/\', array(
    \'core_path\' => $corePath
));

switch ($modx->event->name) {
    case \'OnManagerPageBeforeRender\':
        $modx->controller->addLexiconTopic(\'superboxselect:default\');
        $tvId = isset($modx->controller->scriptProperties[\'id\']) ? $modx->controller->scriptProperties[\'id\'] : 0;
        /** @var modTemplateVar $tv */
        $tv = $modx->getObject(\'modTemplateVar\', $tvId);
        if ($tv) {
            $tvProperties = $tv->get(\'input_properties\');
            $package = isset($tvProperties[\'selectPackage\']) ? $tvProperties[\'selectPackage\'] : \'\';
        } else {
            $package = \'\';
        }
        $superboxselect->includeScriptAssets($package);
        break;
    case \'OnTVInputRenderList\':
        $modx->event->output($corePath . \'elements/tv/input/\');
        break;
    case \'OnTVInputPropertiesList\':
        $modx->event->output($corePath . \'elements/tv/input/options/\');
        break;
    case \'OnDocFormRender\':
        $superboxselect->includeScriptAssets();
        break;
};',
    ),
  ),
  'e7ef558f177d69e939595817fb097e94' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnManagerPageBeforeRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'c65829c95356b35647df72903e9785b3' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputPropertiesList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  'd88523763d4acbac170c865a43182944' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnTVInputRenderList',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
  '895f07af7cc975f8b8d9199ecff5a43c' => 
  array (
    'criteria' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
    ),
    'object' => 
    array (
      'pluginid' => 19,
      'event' => 'OnDocFormRender',
      'priority' => 0,
      'propertyset' => 0,
    ),
  ),
);