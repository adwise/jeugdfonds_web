<?php

class adwisePlugin {

	/** @var \modX $modx */
	public $modx;

	/** @var \Adwise $adwise */
	public $adwise;

	/** @var array $aBefore */
	public $aBefore = array();

	/** @var array $aAfter */
	public $aAfter = array();

	public function __construct(modX &$modx, AdwiseCore &$adwise, array $aConfiguration = array()) {
		$this->modx =& $modx;
		$this->adwise =& $adwise;
	}

	public function compare($oPlugin) {
		$sMyName = get_class($this);
		$sTheirName = get_class($oPlugin);

		if (in_array($sTheirName, $this->aBefore) || in_array($sMyName, $oPlugin->aAfter)) {
			return -1;
		}
		if (in_array($sTheirName, $this->aAfter) || in_array($sMyName, $oPlugin->aBefore)) {
			return 1;
		}

		return strnatcmp($sMyName, $sTheirName);
	}
}