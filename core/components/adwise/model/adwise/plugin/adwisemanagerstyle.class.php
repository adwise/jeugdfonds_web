<?php

class adwiseManagerStyle extends adwisePlugin {

	public function onBeforeManagerPageInit() {
		$this->modx->regClientStartupScript($this->adwise->option(Adwise::alias . '.url_static') . 'adwise/script/manager/adwise.js');
		$this->modx->regClientCSS($this->adwise->option(Adwise::alias . '.url_static') . 'adwise/style/manager/adwise.css');
	}

	public function onManagerPageAfterRender(modSystemEvent &$event, array $aProperties = array()) {
		/** @var modManagerController $oController */
		$oController =& $aProperties['controller'];
		if (empty($oController))
			return;

		switch(get_class($oController)) {
			case 'SecurityLoginManagerController':
				array_unshift(
					$oController->templatesPaths,
					dirname(dirname(dirname(dirname(__FILE__)))) . '/elements/template/manager/'
				);

				$this->modx->smarty->assign('release_year', $this->adwise->option(Adwise::alias . '.release_year', null, date('Y')));
				$this->modx->smarty->assign('copyright_now', date('Y'));

				$oController->content = $oController->fetchTemplate('security/login.tpl');
				break;
		}
	}
}