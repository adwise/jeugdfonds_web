<?php

class adwiseCacheClear extends adwisePlugin {

	public function onSiteRefresh() {
		$this->modx->log(Modx::LOG_LEVEL_INFO, '[Adwise Cache Manager] Clearing minify cache...');

		/** @var adwiseMinify $oMinify */
		$oMinify = $this->adwise->initClass('adwiseMinify');

		$aExtensions = array(
			'js',
			'css'
		);

		foreach($aExtensions as $sExtension) {
			$sCachePath = $oMinify->_getCachePath($sExtension);
			foreach(scandir($sCachePath) as $sFile) {
				if (substr($sFile, -1 * strlen($sExtension)) == $sExtension) {
					unlink($sCachePath . $sFile);
				}
			}
		}
	}
}