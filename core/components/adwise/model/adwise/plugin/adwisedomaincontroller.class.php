<?php

class adwiseDomainController extends adwisePlugin {

	public $aBefore = array(
		'adwiseConfigurableFix'
	);

	public function onInitCulture() {
		if (!empty($this->modx->context) && $this->modx->context->get('key') != 'mgr') {
			$sHost = str_replace('www.', '', isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
			if (!empty($sHost)) {
				$sSql = '
					SELECT `context_key`
					FROM' . $this->modx->getTableName('modContextSetting') . '
					WHERE
						replace(`value`, "{' . Adwise::alias . '.domain_suffix}", "' . $this->adwise->option(Adwise::alias . '.domain_suffix') . '") = "' . $sHost . '"
						AND `context_key` != "mgr"
						AND `namespace` = "adwise"
						AND `key` = "' . Adwise::alias . '.domain_controller"
				';

				$sKey = false;

				/** @var PDOStatement $statement */
				$statement = $this->modx->prepare($sSql);
				if ($statement && $statement->execute())
					$sKey = $statement->fetch(PDO::FETCH_COLUMN);

				if (!empty($sKey) && $this->modx->context->get('key') !== $sKey) {
					$this->modx->switchContext($sKey);

					$this->modx->user = null;
					$this->modx->getUser($sKey);
				}
			}
		}
	}
}
