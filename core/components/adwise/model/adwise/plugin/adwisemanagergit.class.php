<?php

class adwiseManagerGit extends adwisePlugin {

	public function onBeforeManagerPageInit() {
		$user = $this->modx->getUser();
		if ($user->isMember('Administrator')) {
			$configFile = $this->getConfigFile();
			if (!empty($configFile)) {
				$repoName = $this->getRepoName($configFile);

				$this->modx->regClientStartupHTMLBlock(
					'
<div id="git-warning">
    Project in GIT: ' . ($repoName ? '<a href="https://bitbucket.org/adwise/' . $repoName . '" target="_blank">' . $repoName . '</a>' : 'onbekend') . '
</div>
<style>
#git-warning {
    color: #fff;
    background-color: red;
    text-align: center;
    position: absolute;
    left: 50%;
    top: 0;
    width: auto;
    transform: translateX(0);
    padding: 2px 6px;
    border-radius: 0 0 5px 5px;
    font-size: 12px;
    display:inline-block;
    z-index:99999;
}

#git-warning a{
    color: #fff;
    text-decoration: none;
}
</style>'
				);
			}
		}
	}

	private function getConfigFile(): ?string {
		$configFile = dirname(MODX_CORE_PATH) . DIRECTORY_SEPARATOR . '.git' . DIRECTORY_SEPARATOR . 'config';
		if (file_exists($configFile)) {
			return $configFile;
		}

		return null;
	}

	private function getRepoName(string $configFile): ?string {
		$repoName = null;

		$config = file_get_contents($configFile);

		$matches = [];
		if (preg_match('/bitbucket\.org:adwise\/([a-zA-Z0-9\-\._]+)\.git/', $config, $matches)) {
			$repoName = $matches[1];
		}

		return $repoName;
	}
}
