<?php

class adwisePlaceholders extends adwisePlugin {

	public $aBefore = array(
		'adwiseConfigurableFix'
	);

	public function OnInitCulture(modSystemEvent &$event, array $aProperties = array()) {
		$date = new DateTime();

		$this->modx->toPlaceholders(array('now' => $date->getTimestamp()), Adwise::alias);
	}
}