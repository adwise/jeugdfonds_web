<?php
/** @var Adwise $adwise */
$adwise =& $modx->adwise;

$files = $adwise->option('files:csv:trim', $scriptProperties, null);

$output = $adwise->option('output:bool', $scriptProperties, empty($uncompressed));
$combine = $adwise->option('combine:bool', $scriptProperties, true);
$compress = $adwise->option('compress:bool', $scriptProperties, true);
$inline = $adwise->option('inline:bool', $scriptProperties, false);

if ($adwise->option(Adwise::alias .'.development:bool') || !$adwise->option(Adwise::alias .'.optim_js', null, false))
	$combine = $compress = false;

// init session array
if (!isset($_SESSION['adwise']))
	$_SESSION['adwise'] = array();
if (!isset($_SESSION['adwise']['minify']))
	$_SESSION['adwise']['minify'] = array();
if (!isset($_SESSION['adwise']['minify']['js']))
	$_SESSION['adwise']['minify']['js'] = array();
$session =& $_SESSION['adwise']['minify']['js'];

// add uncompressed files
if (!empty($files)) {
	foreach($files as $file) {
		if (!in_array($file, $session))
			$session[] = $file;
	}
}

// output script tags with (compressed) javascript
if ($output) {
	/** @var AdwiseMinify $minify */
	$minify = $adwise->initClass('adwiseMinify');

	if ($combine || $compress)
		$files = $minify->minify($session, 'js', $combine, $compress);
	else
		$files =& $session;

	$output = '';
	foreach($files as $file) {
		if (!empty($file))
			if ($inline)
				$output .= '<script type="text/javascript">' . trim(file_get_contents($file)) . '</script>' . "\n";
			else
				$output .= '<script type="text/javascript" src="' . $file . '"></script>' . "\n";
	}

	// clear session
	$session = array();

	return $output;
}