<?php
/** @var fiHooks $hook */

$bValid = false;

if (isset($_POST['csrf']) && isset($_SESSION['csrf']) && is_array($_SESSION['csrf'])) {
	foreach($_SESSION['csrf'] as $i => $aItem) {
		if (isset($aItem['hash']) && isset($aItem['generated']) && $_POST['csrf'] == $aItem['hash'] && intval($aItem['generated']) > time() - 60 * 30) {
			unset($_SESSION['csrf'][$i]);
			$bValid = true;
			break;
		}
	}
}

if (!$bValid) {
	/** @var fiValidator $oValidator */
	$oValidator =& $hook->formit->request->validator;

	$oValidator->config['validationErrorMessage'] = 'Uw aanvraag kon niet worden behandeld. Probeer het opnieuw.';
	$oValidator->processErrors();

	return false;
}

return true;