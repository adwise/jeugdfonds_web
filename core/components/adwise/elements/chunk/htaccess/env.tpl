<!--@f:off-->
# Static subdomain resolving for "[[+url]]"
<IfModule mod_env.c>
	<IfModule mod_setenvif.c>
		SetEnvIf Host .*[[+url:replace=`.==\.`]].* ADW_STATICKEY=[[+static.key]]
		SetEnvIf Host .*[[+url:replace=`.==\.`]].* ADW_STATICURL=[[+static.url]]
	</IfModule>
</IfModule>
<!--@f:on-->