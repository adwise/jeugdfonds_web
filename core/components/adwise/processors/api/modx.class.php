<?php

class modxREST extends REST {

	private $modx = null;

	public static function checkRoot() {
		$files = array(
			WEBSITE_ROOT . DS . 'core' . DS => WEBSITE_ROOT,
			dirname(WEBSITE_ROOT) . DS . 'core' . DS => dirname(WEBSITE_ROOT)
		);

		foreach($files as $file => $root) {
			if (file_exists($file))
				return $root;
		}

		return false;
	}

	function __construct(modX & $modx = null) {
		parent::__construct();

		if (!empty($modx)) {
			$this->modx = $modx;
		}
		else {
			if (!defined('MODX_CORE_DIR'))
				define('MODX_CORE_DIR', modxREST::checkRoot() . DS . 'core' . DS);

			require_once(MODX_CORE_DIR . 'model' . DS . 'modx' . DS . 'modx.class.php');

			$this->modx = new modX();
			$this->modx->initialize('mgr');
		}
	}

	public function getVersion() {
		$version = $this->modx->getVersionData();

		$this->response(
			array(
				'type' => 'modx',
				'name' => $version['full_appname'],
				'version' => $version['version'] . '.' . $version['major_version'] . '.' . $version['minor_version'],
				'release' => $version['patch_level']
			)
		);
	}

	public function getModules() {
		$pkgList = $this->modx->call(
			'transport.modTransportPackage',
			'listPackages',
			array(
				&$this->modx,
				1,
				0,
				0,
				''
			)
		);

		$returnValue = array();
		foreach($pkgList['collection'] as $key => $pkg) {
			$package = array();
			foreach(unserialize($pkg->_fields['metadata']) as $data) {
				switch($data['name']) {
					case 'name' :
						$package['name'] = $data['text'];
						break;
					case 'version' :
						$package['version'] = $data['text'];
						break;
					case 'release' :
						$package['release'] = $data['text'];
						break;
				}
			}
			if (empty($package)) {
				$package['name'] = $pkg->_fields['package_name'];
				$package['version'] = $pkg->_fields['version_major'] . '.' . $pkg->_fields['version_minor'] . '.' . $pkg->_fields['version_patch'];
				$package['release'] = $pkg->_fields['release'];
			}
			$returnValue[] = $package;
		}
		$this->response($returnValue);
	}

	public function updatePassword() {
		$user = $this->modx->getObject('modUser', array('username' => $this->_request['username']));
		if (!$user) {
			$this->response(
				array(
					'status' => 'error',
					'message' => 'user not found'
				)
			);
		}
		else {
			if ($user->changePassword($this->_request['password-new'], $this->_request['password'])) {
				$this->response(
					array(
						'status' => 'ok',
						'message' => 'password updated'
					)
				);
			}
			else {
				$this->response(
					array(
						'status' => 'error',
						'message' => 'password not updated'
					)
				);
			}
		}
	}

}
