<?php
/** @var modX $modx */
/** @var Adwise $adwise */
/** @var adwScriptProcessor $script */

/** @var adwiseSchemaBuilder $schemaBuilder */
$schemaBuilder = $adwise->initClass('schema.adwiseSchemaBuilder');

$sComponent = $adwise->option('component', $_GET, false);

if (empty($sComponent))
	$script->log('Please define the component', __LINE__, __FILE__);

else
	$schemaBuilder->init($this->modx->getOption('core_path') . 'components/' . $sComponent . '/model/', $sComponent)->build(true, true);