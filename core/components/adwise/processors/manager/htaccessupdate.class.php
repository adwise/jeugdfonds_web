<?php

class adwHtaccessUpdate extends modObjectProcessor {

	/** @var Adwise $adwise */
	public $adwise = null;

	public function __construct(modX & $modx, array $properties = array()) {
		parent::__construct($modx, $properties);

		$this->adwise =& $this->modx->adwise;
	}

	public function process() {
		$this->modx->log(MODX_LOG_LEVEL_INFO, '[Adwise .htaccess Update] Updating .htaccess...');

		$aGenerated = array();

		$aContexts = $this->modx->getCollection('modContext', array('key:!=' => 'mgr'));
		foreach($aContexts as $oContext) {
			$aConfig = $this->adwise->config($oContext->get('key'));

			$sDefaultHost = strpos($_SERVER['HTTP_HOST'], 'www.') === 0 ? substr($_SERVER['HTTP_HOST'], 4) : $_SERVER['HTTP_HOST'];

			$aParams = array(
				'url' => $this->adwise->option(
					Adwise::alias . '.domain_controller',
					$aConfig,
					$this->adwise->option(Adwise::alias . '.domain_controller', null, $sDefaultHost)
				),
				'www' => $this->adwise->option(Adwise::alias . '.htaccess_www', $aConfig, 0),
				'ssl' => $this->adwise->option(Adwise::alias . '.htaccess_ssl', $aConfig, 0),
				'static' => array(
					'key' => $this->adwise->option(Adwise::alias . '.static_context_key', $aConfig, 0),
					'url' => $this->adwise->option(Adwise::alias . '.url_static_context', $aConfig, 0)
				)
			);

			$aGenerated[] = $this->modx->getChunk('htaccess.www', $aParams);
			$aGenerated[] = $this->modx->getChunk('htaccess.ssl', $aParams);
			$aGenerated[] = $this->modx->getChunk('htaccess.env', $aParams);
		}

		$this->_mergeHtaccessFile($aGenerated, MODX_BASE_PATH . '.htaccess');

		$this->modx->log(MODX_LOG_LEVEL_INFO, 'Done...');

		sleep(2);

		return $this->success(
			'',
			array()
		);
	}

	private function _mergeHtaccessFile($aGenterated, $sFile) {
		$sSeparator = '# GENERATED';

		$aParts = array(array());
		$i = 0;

		$aInput = explode("\n", file_exists($sFile) ? file_get_contents($sFile) : '');
		foreach($aInput as $sLine) {
			if (substr($sLine, 0, strlen($sSeparator)) == $sSeparator) {
				if ($i <= 2) {
					$aParts[++$i] = array();
					continue;
				}
			}
			$aParts[$i][] = $sLine;
		}

		array_unshift($aGenterated, $sSeparator . ' (start)');
		array_push($aGenterated, $sSeparator . ' (end)');

		if (sizeof($aParts) == 1)
			$aOutput = array_merge($aGenterated, array(''), $aParts[0]);
		else if (sizeof($aParts) == 2)
			$aOutput = array_merge($aParts[0], $aGenterated);
		else
			$aOutput = array_merge($aParts[0], $aGenterated, $aParts[2]);

		$sOutput = preg_replace('/<!--@f:(on|off)-->/mis', '', implode("\n", $aOutput));
		while(strpos($sOutput, "\n\n\n") !== false) {
			$sOutput = str_replace("\n\n\n", "\n\n", $sOutput);
		}

		file_put_contents($sFile, $sOutput);
	}
}

return 'adwHtaccessUpdate';