<?php

$_lang['adwise.menu'] = 'Adwise Menu';

$_lang['adwise.menu_optim_images'] = 'Optimise Images';
$_lang['adwise.menu_optim_images_desc'] = 'Optimise all images in the \'static\' directory.';

$_lang['adwise.menu_optim_images_clear_history'] = 'Clear History';
$_lang['adwise.menu_optim_images_clear_history_desc'] = 'Clear the optimisation history.';

$_lang['adwise.menu_htaccess_update'] = 'Update .htaccess Files';
$_lang['adwise.menu_htaccess_update_desc'] = 'Update .htaccess files from context- and systemsettings.';
