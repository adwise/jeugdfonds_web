<?php

/* Contact form */
$_lang['Send'] = 'Verzenden';

/* Mixed Contact form / Email report */
$_lang['Name'] = 'Je naam';
$_lang['Firstname'] = 'Voornaam';
$_lang['Lastname'] = 'Achternaam';
$_lang['Email'] = 'Je e-mailadres';
$_lang['email'] = 'E-mailadres';
$_lang['Telephone']  = 'Telefoonnummer';
$_lang['Subject']  = 'Je bericht of vraag';
$_lang['Text'] = 'Bericht';
$_lang['Voor- en achternaam'] = 'Voor- en achternaam';

/* Email report */
$_lang['email_header-text'] = 'Op [[!+adw.now:date=`%d-%m-%Y`]] om [[!+adw.now:date=`%H:%M`]] heeft u een contactverzoek naar ons via onze <a href="[[++site_name]]" target="_blank" style="color: #08512a; text-decoration: none;">website</a> verzonden. Het verzoek is als volgt:';
$_lang['email_header-header'] = 'Aanvraag van pagina "[[*pagetitle]]"';

