$(document).ready(function() {

	// form validation
	$('form.validate').each(function() {
		$(this).validate({
			errorElement: 'span',
			errorPlacement: function(error, element) {
				error.appendTo($(element).parents('.field'));
			},
			highlight: function(element) {
				$(element).parents('.field').addClass('error');
			},
			success: function(element) {
				$(element).addClass('valid');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.field').removeClass('error');
			}
		});
	});

	// desktop / mobile template force links
	$('#device-force').on('click', function() {
		$.cookie('device_force', $(this).data('type'), {expires: 365, path: '/'});
		location.reload();
	});
});