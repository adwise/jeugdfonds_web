<head>
	{$_modx->config['adw.meta_copyright']}

	<style>
		{ignore}html{visibility: hidden}{/ignore}
	</style>

	{if $_modx->context.seo_google_gtm != 'GTM-ADWISE'}
		<script type="text/javascript" charset="utf-8" data-skip="1">
			(function(w,d,s,l,i){
				w[l]=w[l]||[];w[l].push({ 'gtm.start':new Date().getTime(),event:'gtm.js'});
				var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','{$_modx->config['adw.seo_google_gtm']}');
		</script>
	{/if}

	<script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "address": {
                "@type": "PostalAddress",
                "addressLocality": "{$_modx->config['adw.contact_city']}",
                "addressRegion": "{$_modx->config['adw.contact_state']}",
                "postalCode": "{$_modx->config['adw.contact_postalcode']}",
                "streetAddress": "{$_modx->config['adw.contact_address']}"
            },
            "name": "{$_modx->context.site_name}",
            "telephone": "{$_modx->config['adw.contact_telephone_general']}",
            "url": "{$_modx->config.site_url}"
        }
    </script>

	<base href="{$_modx->config.site_url}"/>
	<title>{$_modx->config['adw.seo_meta_title']}</title>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<meta name="description" content="{$_modx->resource.description?: $_modx->config['adw.seo_meta_description']}"/>
	<meta name="author" content="{$_modx->config['adw.seo_meta_author']}"/>
	<meta name="robots" content="{$_modx->config['adw.seo_meta_robots']?: $_modx->config['adw.seo_meta_robots']}">

	<meta property="og:locale" content="{$_modx->config.locale|replace:".utf8":""}"/>

	<meta property="og:type" content="{if $_modx->config.site_start == $_modx->resource.id}website{else}article{/if}"/>
	<meta property="og:title" content="{$_modx->config['adw.seo_meta_title']}"/>
	<meta property="og:description" content="{$_modx->resource.description?: $_modx->config['adw.seo_meta_description']}"/>
	<meta property="og:url" content="{$_modx->resource.id | url}"/>
	<meta property="og:site_name" content="{$_modx->config.site_name}"/>
	<meta property="og:image" content="{'ImagePlus' | snippet : [ 'value' => $_modx->resource.media_overview_image? : '/static/default/media/svg/logo.jpg', 'options' => 'w=1200&h=628&zc=1' ]}"/>

	<meta property="fb:admins" content=""/>
	<meta name="twitter:card" content="summary_large_image"/>
	<meta name="twitter:description" content="{$_modx->config['adw.seo_meta_description']?: $_modx->config['adw.seo_meta_description']}"/>
	<meta name="twitter:title" content="{$_modx->config['adw.seo_meta_title']}"/>
	<meta name="twitter:site" content="@Adwise_BV"/>
	<meta name="twitter:domain" content="{$_modx->config.site_name}"/>

	<meta name="twitter:image" content="{'ImagePlus' | snippet : [ 'value' => $_modx->resource.media_overview_image? : '/static/default/media/images/templating/social/logo-share.jpg', 'options' => 'w=520&h=254&zc=1' ]}"/>
	<meta name="twitter:creator" content="@Adwise_BV"/>

	<link rel="apple-touch-icon" sizes="180x180" href="/static/default/media/icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/static/default/media/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/static/default/media/icons/favicon-16x16.png">
	<link rel="manifest" href="/static/default/media/icons/site.webmanifest">
	<link rel="mask-icon" href="/static/default/media/icons/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/static/default/media/icons/favicon.ico">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-config" content="/static/default/media/icons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="{$_modx->config['adw.url_static']}default/style/screen.min.css?v={$_modx->config['adw.general-release-version']}"/>

</head>


