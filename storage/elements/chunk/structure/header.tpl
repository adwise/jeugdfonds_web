{set $rid = $_modx->resource.id}
<div class="mobile">
    <button value="sluit navigatie" class="mobile__menu-close"><span>Sluiten</span></button>
    {'pdoMenu' | snippet : [
    'startId' => 0,
    'level' => 3,
    'outerClass' => 'mobile__menu-list',
    'innerClass' => 'mobile__menu-sublist',
    'parentClass' => 'open-submenu',
    'levelClass' => 'level-'
    ]}
    <img class="mobile__menu-oval" src="/static/default/media/svg/oval.svg" alt="circle">
    <img class="mobile__menu-triangle" src="/static/default/media/svg/triangle.svg" alt="triangle">
    <img class="mobile__menu-rectangle" src="/static/default/media/svg/rectangle.svg" alt="rectangle">
</div>
<div class="mobile__background"></div>

<header class="nav">
    <div class="grid-container">
        <div class="nav__mobile grid-x">
            <div class="nav-mobile-logo small-6">
                {if $rid != 1}
                    <a href="{$_modx->config.site_url}" title="{$_modx->config.site_name}">
                        <img src="/static/default/media/svg/logo.svg" alt="{$_modx->config.site_name}" class="logo">
                    </a>
                {else}
                    <img src="/static/default/media/svg/logo.svg" alt="{$_modx->config.site_name}" class="logo">
                {/if}
            </div>
            <div class="nav__mobile-hamburger small-6">
                <button value="open menu">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                    MENU
                </button>
            </div>
        </div>
        <div class="nav__desktop grid-x">
            <div class="nav__desktop-logo xlarge-3">
                {if $rid != 1}
                <a href="{$_modx->config.site_url}" title="{$_modx->config.site_name}">
                    <img src="/static/default/media/svg/logo.svg" alt="{$_modx->config.site_name}" class="logo">
                </a>
                {else}
                    <img src="/static/default/media/svg/logo.svg" alt="{$_modx->config.site_name}" class="logo">
                {/if}
            </div>
            <nav class="nav__desktop-menu xlarge-9">
                {'pdoMenu' | snippet : [
                'startId' => 0,
                'level' => 2,
                'outerClass' => 'wrapper grid-x no-bullet',
                'innerClass' => 'submenu no-bullet',
                'parentClass' => 'open-submenu'
                ]}
            </nav>
            {set $currentResource = $_modx->resource.id}
            {if $currentResource != 1}
            <div class="nav__desktop-breadcrumbs xlarge-10 grid-x">
                {'pdoCrumbs' | snippet :[
                'showHome' => 1,
                'tpl' => 'breadcrumbs-row',
                'tplHome'=>'breadcrumbs-home-row',
                'tplCurrent'=>'breadcrumbs-current-row'
                ]}
            </div>
            {/if}
        </div>
    </div>
</header>
