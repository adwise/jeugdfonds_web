<footer class="footer">
    <div class="grid-container">
        <div class="footer__top grid-x grid-margin-x grid-margin-y">

            <div class="footer__top-contact cell small-12 large-3">
                <img class="logo" src="/static/default/media/svg/logo-white.svg" alt="{$_modx->config.site_name}">
                <address>
                    <p>{$_modx->config['footer_address-name']}</p>
                    <p>{$_modx->config['footer_address-street']}</p>
                    <p>{$_modx->config['footer_address-postcode']}</p>
                </address>
                <div class="grid-y contact">
                    {*<a class="phone" href="tel:{$_modx->config['footer_contact-phone']}">{$_modx->config['footer_contact-phone']}</a>*}
                    <a class="mail" href="mailto:{$_modx->config['footer_contact-email']}">{$_modx->config['footer_contact-email']}</a>
                </div>
                <a class="facebook" href="https://www.facebook.com/stichtingjeugdfondsalmelo/ " title="Facebook">
                    <span>Volg ons op:</span>
                    <svg width="25px" height="25px" viewBox="0 0 72 72" id="emoji" xmlns="http://www.w3.org/2000/svg">
                        <g id="color">
                            <path fill="#3FC3DA" stroke="none" d="M57,12H15c-2.2091,0-4,1.7909-4,4v42c0,2.2091,1.7909,4,4,4h42c2.2091,0,4-1.7909,4-4V16 C61,13.7909,59.2091,12,57,12z"/>
                            <path fill="#FFFFFF" stroke="none" d="M48.5,26.8438c0.7812,0,4.6875,0,4.6875,0v-7.0312c-3.125,0-15.625-3.125-15.625,10.1562v4.6875h-6.25 v7.8125h6.25V63h7.8125V42.4688h7.0312l0.7812-7.8125H45.375v-4.6875C45.375,26.8438,47.7188,26.8438,48.5,26.8438z"/>
                        </g>
                        <g id="hair"/>
                        <g id="skin"/>
                        <g id="skin-shadow"/>
                        <g id="line">
                            <path fill="none" stroke="#000000" stroke-miterlimit="10" stroke-width="2" d="M45.6188,62h12.1554 C59.5558,62,61,60.5558,61,58.7742V15.2258C61,13.4442,59.5558,12,57.7742,12H14.2258C12.4442,12,11,13.4442,11,15.2258v43.5484 C11,60.5558,12.4442,62,14.2258,62h23.4761"/>
                            <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" d="M45.6183,62V42.208h7.1251l0.7917-7.9168h-7.9168v-4.7501c0-3.1667,2.375-3.1667,3.1667-3.1667s4.7501,0,4.7501,0v-7.1251 c-3.1997-0.3243-15.8336-3.1667-15.8336,10.2918v4.7501h-6.3334v7.9168h6.3334V62"/>
                        </g>
                    </svg>
                </a>
            </div>

            <div class="cell small-4 large-2 large-offset-2 footer__top-links">
                <h2 class="heading-xs">{$_modx->config['footer_columns-left-title']}</h2>
                {'pdoMenu' | snippet : [
                'parents' => 0,
                'resources' => $_modx->config['footer_columns-left-links'],
                'showHidden' => 1,
                'showUnpublished' => 1,
                'outerClass' => 'no-bullet',
                ]}
            </div>

            <div class="cell small-4 large-2 footer__top-links">
                <h2 class="heading-xs">{$_modx->config['footer_columns-middle-title']}</h2>
                {'pdoMenu' | snippet : [
                'parents' => 0,
                'resources' => $_modx->config['footer_columns-middle-links'],
                ['sortby' => 'FIELD(modResource.id, '~$_modx->config['footer_columns-middle-links']~')'],
                'showHidden' => 1,
                'showUnpublished' => 1,
                'outerClass' => 'no-bullet',
                ]}
            </div>

            <div class="cell small-4 large-2 footer__top-links">
                <h2 class="heading-xs">{$_modx->config['footer_columns-right-title']}</h2>
                {'pdoMenu' | snippet : [
                'parents' => 0,
                'resources' => $_modx->config['footer_columns-right-links'],
                'showHidden' => 1,
                'showUnpublished' => 1,
                'outerClass' => 'no-bullet',
                ]}
            </div>

        </div>

        <div class="footer__bottom">
            <div class="footer__bottom-copy">Copyright [[!+nowdate:default=`now`:strtotime:date=`%Y`]] Jeugdfonds Almelo</div>
            <ul class="footer__bottom-items grid-x no-bullet">
                {*<li class="first"><a href="https://jeugdfondsalmelo.nl/service/algemene-voorwaarden/">Algemene Voorwaarden</a></li>*}
                <li><a href="https://jeugdfondsalmelo.nl/service/privacy-statement/">Privacy statement</a></li>
                <li><a href="https://jeugdfondsalmelo.nl/service/cookies-en-cookiebeleid/">Cookies</a></li>
                {*<li><a href="https://jeugdfondsalmelo.nl/service/copyright/">Copyright</a></li>*}
                <li><a href="https://jeugdfondsalmelo.nl/service/disclaimer/">Disclaimer</a></li>
                <li class="last"><a href="https://jeugdfondsalmelo.nl/systeem/sitemap/">Sitemap</a></li>
            </ul>
        </div>

    </div>
    <img class="sun" src="/static/default/media/svg/zon.svg" alt="sun">
</footer>

{include 'file:chunk/cookie/cookie_consent.tpl'}