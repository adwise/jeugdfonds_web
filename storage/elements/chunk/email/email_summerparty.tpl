[[$email_wrapper?
&content=`<h2>Verzonden vanaf pagina [[*pagetitle]]</h2>

<p>Verzonden op [[!+adw.now:date=`%d-%m-%Y`]] om [[!+adw.now:date=`%H:%M`]]</p>

<h3>Bericht:</h3><br />
<br />
Gezinsnummer: [[+gezinsnummer]]<br />
Naam ouder/verzorger: [[+naamouder]]<br />
E-mail ouder/verzorger: [[+emailouder]]<br />
Aantal kaartjes kinderen (3 t/m 17 jaar): [[+kaartjeskinderen]]<br />
Aantal kaartjes volwassen: [[+kaartjesvolwassen]]<br />

<p>[[+text:striptags:nl2br]]</p>
`]]