[[$email_wrapper?
&subject=`Aanvraag website`
&content=`<h2>Verzonden vanaf pagina [[*pagetitle]]</h2>

<p>Verzonden op [[!+adw.now:date=`%d-%m-%Y`]] om [[!+adw.now:date=`%H:%M`]]</p>
<p>Is vanaf <strong>[[+previous]]</strong> op het aanvraag formulier uitgekomen</p>

<table>

    <tr class="field">
        <td width="200"><strong>Naam:</strong></td>
        <td>[[+name]]</td>
    </tr>

    <tr class="field">
        <td width="200"><strong>Straatnaam en huisnummer:</strong></td>
        <td>[[+street]]</td>
    </tr>

    <tr class="field">
        <td width="200"><strong>Postcode:</strong></td>
        <td>[[+postcode]]</td>
    </tr>

    <tr class="field">
        <td width="200"><strong>Plaatsnaam:</strong></td>
        <td>[[+city]]</td>
    </tr>

    [[+telephone:notempty=`<tr class="field">
        <td width="200"><strong>Telefoon:</strong></td>
        <td><span class="mobile_link">[[+telephone]]</span></td>
    </tr>`]]

    <tr class="field">
        <td width="200"><strong>E-mailadres:</strong></td>
        <td><a href="mailto:[[+email]]" target="_blank" style="color: gold; text-decoration: none;">[[+email]]</a></td>
    </tr>

</table>

<h3 class="field">Toelichting:</h3>

<p>[[+text:striptags:nl2br]]</p>
`]]