[[$email_wrapper?
&subject=`Contactaanvraag website`
&content=`<h2>Verzonden vanaf pagina [[*pagetitle]]</h2>

<p>Verzonden op [[!+adw.now:date=`%d-%m-%Y`]] om [[!+adw.now:date=`%H:%M`]]</p>
<p>Is vanaf <strong>[[+previous]]</strong> op het contact formulier uitgekomen</p>

<table>

    <tr class="field">
        <td width="200"><strong>Naam:</strong></td>
        <td>[[+name]]</td>
    </tr>

    <tr class="field">
        <td width="200"><strong>Aanhef:</strong></td>
        <td>[[+aanhef]]</td>
    </tr>

    <tr class="field">
        <td width="200"><strong>E-mailadres:</strong></td>
        <td><a href="mailto:[[+email]]" target="_blank" style="color: gold; text-decoration: none;">[[+email]]</a></td>
    </tr>

    [[+phone:notempty=`<tr class="field">
        <td width="200"><strong>Telefoon:</strong></td>
        <td><span class="mobile_link">[[+phone]]</span></td>
    </tr>`]]

    [[+0:notempty=`<tr class="field">
        <td width="200"><strong>Onderwerp:</strong></td>
        <td>
        [[+0:notempty=`[[+0]]`]] <br />
        [[+1:notempty=`[[+1]]`]] <br />
        [[+2:notempty=`[[+2]]`]] <br />
        [[+3:notempty=`[[+3]]`]] <br />
        [[+4:notempty=`[[+4]]`]] <br />
        [[+5:notempty=`[[+5]]`]] <br />
        [[+6:notempty=`[[+6]]`]] <br />
        [[+7:notempty=`[[+7]]`]] <br />
        [[+8:notempty=`[[+8]]`]] <br />
        [[+9:notempty=`[[+9]]`]] <br />
        [[+10:notempty=`[[+10]]`]] <br />
        </td>
    </tr>`]]

</table>

<h3 class="field">Bericht:</h3>

<p>[[+text:striptags:nl2br]]</p>
`]]