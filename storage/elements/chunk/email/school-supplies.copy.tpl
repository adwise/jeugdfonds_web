[[$email_wrapper?
&content=`<p>Hierbij ontvangt u de bevestiging dat u de schoolspullenpas voor uw kind(eren) hebt aangevraagd.</p>
<p>Belangrijke informatie:</p>
<p><ol><li>Een schoolspullenpas is alleen voor:</li><p>
<ol type="a">
<li>Leerlingen die naar het voorgezet (speciaal) onderwijs gaan of</li>
<li>Leerlingen die naar het mbo of hbo gaan en nog geen 18 zijn!</li>
</ol>
<p><li>U ontvangt de schoolspullenpas voor 1 juli met de post.</li></p>
<p><li>Aanmeldingen zonder gezinsnummer kunnen niet in behandeling worden genomen. In dat geval kunt u via het algemene aanvraagformulier uw gezinsnummer opvragen. Als u nog helemaal niet bekend bent met Jeugdfonds Almelo (Leergeld!) dan kunt u ook het algemene aanvraagformulier invullen.</li></p>
</ol>
`]]