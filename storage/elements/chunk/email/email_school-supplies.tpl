[[$email_wrapper?
&content=`<h2>Verzonden vanaf pagina [[*pagetitle]]</h2>

<p>Verzonden op [[!+adw.now:date=`%d-%m-%Y`]] om [[!+adw.now:date=`%H:%M`]]</p>

<strong>Details:</strong>

<h3>Bericht:</h3><br />
<br />
Gezinsnummer: [[+gezinsnummer]]<br />
Naam ouder/verzorger: [[+naamouder]]<br />
E-mail ouder/verzorger: [[+emailouder]]<br />

<h4>Kind 1:</h4><br />
<br />
Voornaam kind 1: [[+kind1voornaam]]<br />
Achternaam kind 1: [[+kind1achternaam]]<br />
Geboortedatum kind 1: [[+kind1geboortedatum]]<br />
School kind 1: [[+kind1school]]<br />
Andere school kind 1: [[+andere-school1]]<br />
Huidige klas kind 1: [[+kind1klas]]<br />
Klas na zomervakantie kind 1: [[+kind1klaszomervakantie]]<br />
Opmerking kind 1: [[+opmerkingen1]]<br />

<h4>Kind 2:</h4><br />
<br />
Voornaam kind 2: [[+kind2voornaam]]<br />
Achternaam kind 2: [[+kind2achternaam]]<br />
Geboortedatum kind 2: [[+kind2geboortedatum]]<br />
School kind 2: [[+kind2school]]<br />
Andere school kind 2: [[+andere-school2]]<br />
Huidige klas kind 2: [[+kind2klas]]<br />
Klas na zomervakantie kind 2: [[+kind2klaszomervakantie]]<br />
Opmerking kind 2: [[+opmerkingen2]]<br />

<h4>Kind 3:</h4><br />
<br />
Voornaam kind 3: [[+kind3voornaam]]<br />
Achternaam kind 3: [[+kind3achternaam]]<br />
Geboortedatum kind 3: [[+kind3geboortedatum]]<br />
School kind 3: [[+kind3school]]<br />
Andere school kind 3: [[+andere-school3]]<br />
Huidige klas kind 3: [[+kind3klas]]<br />
Klas na zomervakantie kind 3: [[+kind3klaszomervakantie]]<br />
Opmerking kind 3: [[+opmerkingen3]]<br />

<h4>Kind 4:</h4><br />
<br />
Voornaam kind 4: [[+kind4voornaam]]<br />
Achternaam kind 4: [[+kind4achternaam]]<br />
Geboortedatum kind 4: [[+kind4geboortedatum]]<br />
School kind 4: [[+kind4school]]<br />
Andere school kind 4: [[+andere-school4]]<br />
Huidige klas kind 4: [[+kind4klas]]<br />
Klas na zomervakantie kind 4: [[+kind4klaszomervakantie]]<br />
Opmerking kind 4: [[+opmerkingen4]]<br />

<p>[[+text:striptags:nl2br]]</p>
`]]