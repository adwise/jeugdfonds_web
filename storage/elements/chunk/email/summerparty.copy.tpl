[[$email_wrapper?
&content=`<p>Hierbij ontvangt u de bevestiging dat u het zomeruitje heeft aangevraagd. U ontvangt uiterlijk 22 juli 2024 een mail van ons met alle informatie.</p>
<p>Heeft u een aanvraag gedaan zonder een juist gezinsnummer dan wordt uw aanvraag niet in behandeling genomen!</p>
`]]