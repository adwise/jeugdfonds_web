[[$email_wrapper?
&content=`<h2>[[%Verzonden vanaf pagina]] [[*pagetitle]]</h2>

<p>[[%Verzonden op]] [[!+adw.now:date=`%d-%m-%Y`]] [[%om]] [[!+adw.now:date=`%H:%M`]]</p>

<strong>[[%Details]]:</strong>

<h3>[[%Bericht]]:</h3><br />
<br />
[[%Gezinsnummer]]: [[+gezinsnummer]]<br />
[[%Voornaam]] [[%ouder]]/[[%verzorger]]: [[+voornaamouder]]<br />
[[%Achternaam]] [[%ouder]]/[[%verzorger]]: [[+achternaamouder]]<br />
[[%E-mail]] [[%ouder]]/[[%verzorger]]: [[+email]]<br />
[[%Telefoon]] [[%ouder]]/[[%verzorger]]: [[+telefoonnummer]]<br />
[[%Straatnaam]] [[%ouder]]/[[%verzorger]]: [[+straatnaam]]<br />
[[%Huisnummer]] [[%ouder]]/[[%verzorger]]: [[+huisnummer]]<br />
[[%Postcode]] [[%ouder]]/[[%verzorger]]: [[+postcode]]<br />
[[%Woonplaats]] [[%ouder]]/[[%verzorger]]: [[+plaats]]<br />

<h4>[[%Kind 1]]:</h4><br />
<br />
[[%Geslacht kind]] 1: [[+geslacht1]]<br />
[[%Geslacht optie anders kind]] 1: [[+andere-geslacht1]]<br />
[[%Voornaam kind]] 1: [[+kind1voornaam]]<br />
[[%Achternaam kind]] 1: [[+kind1achternaam]]<br />
[[%Geboortedatum kind]] 1: [[+kind1geboortedatum]]<br />

<h4>[[%Kind 2]]:</h4><br />
<br />
[[%Geslacht kind]] 2: [[+geslacht2]]<br />
[[%Geslacht optie anders kind]] 2: [[+andere-geslacht2]]<br />
[[%Voornaam kind]] 2: [[+kind2voornaam]]<br />
[[%Achternaam kind]] 2: [[+kind2achternaam]]<br />
[[%Geboortedatum kind]] 2: [[+kind2geboortedatum]]<br />

<h4>[[%Kind 3]]:</h4><br />
<br />
[[%Geslacht kind]] 3: [[+geslacht3]]<br />
[[%Geslacht optie anders kind]] 3: [[+andere-geslacht3]]<br />
[[%Voornaam kind]] 3: [[+kind3voornaam]]<br />
[[%Achternaam kind]] 3: [[+kind3achternaam]]<br />
[[%Geboortedatum kind]] 3: [[+kind3geboortedatum]]<br />

<h4>[[%Kind 4]]:</h4><br />
<br />
[[%Geslacht kind]] 4: [[+geslacht4]]<br />
[[%Geslacht optie anders kind]] 4: [[+andere-geslacht4]]<br />
[[%Voornaam kind]] 4: [[+kind4voornaam]]<br />
[[%Achternaam kind]] 4: [[+kind4achternaam]]<br />
[[%Geboortedatum kind]] 4: [[+kind4geboortedatum]]<br />

<p>[[+text:striptags:nl2br]]</p>
`]]