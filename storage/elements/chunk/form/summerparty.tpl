{$_modx->runSnippet('!FormIt@summerparty', [
'redirectTo' => $block.redirect
])}

[[!+fi.successMessage:notempty=`
<div class="alert alert-success"></div>
`]]
<div class="block_form">
    <form method="post" action="" class="form">

        <div class="form-group">
            <label class="col-md-4 control-label" for="gezinsnummer">[[%Gezinsnummer]]</label>
            <div class="form__fields-field cell large-6">
                <input id="gezinsnummer" name="gezinsnummer" type="number" class="form-control input-md" maxlength="4" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="naamouder">[[%Naam]] [[%ouder]]/[[%verzorger]]</label>
            <div class="form__fields-field cell large-6">
                <input id="naamouder" name="naamouder" type="text" class="form-control input-md" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="emailouder">[[%E-mail]] [[%ouder]]/[[%verzorger]]</label>
            <div class="form__fields-field cell large-6">
                <input id="emailouder" name="emailouder" type="email" class="form-control input-md" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="kaartjeskinderen">[[%Aantal kaartjes kinderen (3 t/m 17 jaar)]]</label>
            <div class="form__fields-field cell large-6">
                <input id="kaartjeskinderen" name="kaartjeskinderen" type="number" class="form-control input-md" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="kaartjesvolwassen">[[%Aantal kaartjes volwassen]]</label>
            <div class="form__fields-field cell large-6">
                <input id="kaartjesvolwassen" name="kaartjesvolwassen" type="number" class="form-control input-md" required>
            </div>
        </div>

        [[!recaptchav3_render]]
        [[!+fi.error.recaptchav3_error]]

        <button class="button primary yellow">Verzenden</button>

        <input type="hidden" name="form-zomeruitje" value="Send" />
        <input type="hidden" name="previous" id="previous" value="[[#SERVER.HTTP_REFERER]]" />
        <input type="hidden" name="ghost" value="" />
        <input type="hidden" name="csrf" value="[[!csrf.generate]]" />
        <input type="hidden" name="referral URL" value="[[~[[*id]]? &scheme=`full`]]"/>
        <input type="hidden" name="referral ID" value="[[*id]]"/>
    </form>
</div>