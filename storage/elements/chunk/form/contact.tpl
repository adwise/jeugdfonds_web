{$_modx->runSnippet('!FormIt@contact', [
'redirectTo' => $block.redirect
])}

<div class="alert alert-error">[[!+fi.validation_error_message]]  <pre>[[!+fi.error_message]]</pre></div>
<form method="post" action="" class="validate contact-form [[!+fi.validation_error_message:notempty=`error`]]" accept-charset="utf-8" enctype="application/x-www-form-urlencoded" id="contact-form">

        <div class="form__fields-field cell large-6 [[!+fi.error.name:notempty=`error`]]">
            <label for="form-name" class="required">Voor- en achternaam *</label>
            <input type="text" name="name" id="form-name" class="required" value="[[!+fi.name:esc]]" required oninvalid="this.setCustomValidity('Vul je volledige naam in')" oninput="this.setCustomValidity('')"/>
            [[!+fi.error.name]]
        </div>

        <div class="small-12 cell input-group radio aanhef">
            <label for="aanhef">Aanhef</label>
            <fieldset>
                <label for="heer">de heer</label>
                <input type="radio" name="aanhef" value="heer" id="heer" class="group-check"/>
                <label for="mevrouw">mevrouw</label>
                <input type="radio" name="aanhef" value="mevrouw" id="mevrouw" class="group-check"/>
            </fieldset>
        </div>

        <div class="form__fields-field cell large-6 [[!+fi.error.phone:notempty=`error`]]">
            <label for="form-phone">Telefoonnummer</label>
            <input type="text" name="phone" id="form-phone" class="required" value="[[!+fi.phone:esc]]" />
            [[!+fi.error.phone]]
        </div>

        <div class="form__fields-field cell large-6 [[!+fi.error.email:notempty=`error`]]">
            <label for="form-email" class="required">Je e-mailadres *</label>
            <input type="text" name="email" id="form-email" class="required" value="[[!+fi.email:esc]]" required oninvalid="this.setCustomValidity('Vul je e-mailadres in')" oninput="this.setCustomValidity('')"/>
            [[!+fi.error.email]]
        </div>

    {if $formoptions != ''}
            <div class="small-12 cell input-group radio">
                <fieldset>

                    <p>Ik wil graag informatie over:</p>

                    {set $options = json_decode($formoptions, true)}
                    {foreach $options as $idx => $option}

                        {set $postedvalue = '!getPostVal' | snippet : ['field' => $idx]}
                        <label for="{$idx}">{$option.title}</label>
                        <input type="checkbox" name="{$idx}" value="{$option.title}" id="{$idx}" {$postedvalue == $option.title? 'checked' :''} class="group-check"/>

                    {/foreach}
                </fieldset>
            </div>
    {/if}

    <div class="form__fields-field textarea [[!+fi.error.text:notempty=`error`]]">
        <label for="form-text" class="required">Je bericht of vraag *</label>
        <textarea name="text" id="form-text" class="required" rows="8" required oninvalid="this.setCustomValidity('Schrijf een bericht')" oninput="this.setCustomValidity('')">[[!+fi.text:striptags:esc]]</textarea>
        [[!+fi.error.text]]
    </div>

    [[!recaptchav3_render]]
    [[!+fi.error.recaptchav3_error]]

    <button class="button primary yellow">Verzenden</button>
    <div class="note"><p>Reactie binnen twee weken</p></div>

    <input type="hidden" name="form-contact" value="Send" />
    <input type="hidden" name="previous" id="previous" value="[[#SERVER.HTTP_REFERER]]" />
    <input type="hidden" name="ghost" value="" />
    <input type="hidden" name="csrf" value="[[!csrf.generate]]" />
    <input type="hidden" name="referral URL" value="[[~[[*id]]? &scheme=`full`]]"/>
    <input type="hidden" name="referral ID" value="[[*id]]"/>
</form>
