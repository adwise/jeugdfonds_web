{$_modx->runSnippet('!FormIt@aanvraag', [
'redirectTo' => $block.redirect
])}

<div class="alert alert-error">[[!+fi.validation_error_message]]  <pre>[[!+fi.error_message]]</pre></div>
<form method="post" action="" class="validate aanvraag-form [[!+fi.validation_error_message:notempty=`error`]]" accept-charset="utf-8" enctype="application/x-www-form-urlencoded" id="aanvraag-form">

        <div class="form__fields-field cell large-6 [[!+fi.error.name:notempty=`error`]]">
            <label for="form-name" class="required">Voor- en achternaam *</label>
            <input type="text" name="name" id="form-name" class="required" value="[[!+fi.name:esc]]" required oninvalid="this.setCustomValidity('Vul je volledige naam in')" oninput="this.setCustomValidity('')"/>
            [[!+fi.error.name]]
        </div>

        <div class="form__fields-field cell large-6 [[!+fi.error.street:notempty=`error`]]">
            <label for="form-street" class="required">Straatnaam en huisnummer *</label>
            <input type="text" name="street" id="form-street" class="required" value="[[!+fi.street:esc]]" required oninvalid="this.setCustomValidity('Vul je straatnaam en huisnummer in')" oninput="this.setCustomValidity('')"/>
            [[!+fi.error.street]]
        </div>

        <div class="form__fields-field cell large-6 [[!+fi.error.postcode:notempty=`error`]]">
            <label for="form-postcode" class="required">Postcode *</label>
            <input type="text" name="postcode" id="form-postcode" class="required" value="[[!+fi.postcode:esc]]" required oninvalid="this.setCustomValidity('Vul je postcode in')" oninput="this.setCustomValidity('')"/>
            [[!+fi.error.postcode]]
        </div>

        <div class="form__fields-field cell large-6 [[!+fi.error.city:notempty=`error`]]">
            <label for="form-city" class="required">Plaatsnaam *</label>
            <input type="text" name="city" id="form-city" class="required" value="[[!+fi.city:esc]]" required oninvalid="this.setCustomValidity('Vul je plaatsnaam in')" oninput="this.setCustomValidity('')"/>
            [[!+fi.error.city]]
        </div>

        <div class="form__fields-field cell large-6 [[!+fi.error.telephone:notempty=`error`]]">
            <label for="form-telephone" class="required">Telefoonnummer *</label>
            <input type="text" name="telephone" id="form-telephone" class="required" value="[[!+fi.telephone:esc]]" required oninvalid="this.setCustomValidity('Vul je telefoonnummer in')" oninput="this.setCustomValidity('')"/>
            [[!+fi.error.telephone]]
        </div>

        <div class="form__fields-field cell large-6 [[!+fi.error.email:notempty=`error`]]">
            <label for="form-email" class="required">Je e-mailadres *</label>
            <input type="text" name="email" id="form-email" class="required" value="[[!+fi.email:esc]]" required oninvalid="this.setCustomValidity('Vul je e-mailadres in')" oninput="this.setCustomValidity('')"/>
            [[!+fi.error.email]]
        </div>

        <div class="form__fields-field textarea [[!+fi.error.text:notempty=`error`]]">
            <label for="form-text" class="required">Toelichting *</label>
            <textarea name="text" placeholder="Waarvoor en waarom doe jij deze aanvraag?" id="form-text" class="required" rows="8" required oninvalid="this.setCustomValidity('Schrijf een toelichting')" oninput="this.setCustomValidity('')">[[!+fi.text:striptags:esc]]</textarea>
            [[!+fi.error.text]]
        </div>

        <input type="checkbox" name="privacy" id="form-privacy" class="required" required oninvalid="this.setCustomValidity('Ga akkoord met het privacy reglement')" oninput="this.setCustomValidity('')"/>
        <label for="form-privacy" class="required">Door het invullen van dit aanvraagformulier geeft u aan akkoord te gaan met ons privacy reglement. *</label>

    [[!recaptchav3_render]]
    [[!+fi.error.recaptchav3_error]]

    <button class="button primary yellow">Verzenden</button>
    <div class="note"><p>Reactie binnen twee weken</p></div>

    <input type="hidden" name="aanvragen" value="Send" />
    <input type="hidden" name="previous" id="previous" value="[[#SERVER.HTTP_REFERER]]" />
    <input type="hidden" name="ghost" value="" />
    <input type="hidden" name="csrf" value="[[!csrf.generate]]" />
    <input type="hidden" name="referral URL" value="[[~[[*id]]? &scheme=`full`]]"/>
    <input type="hidden" name="referral ID" value="[[*id]]"/>
</form>
