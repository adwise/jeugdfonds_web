{$_modx->runSnippet('!FormIt@schoolsupplies', [
'redirectTo' => $block.redirect
])}

[[!+fi.successMessage:notempty=`
<div class="alert alert-success"></div>
`]]
<div class="block_form">
    <form method="post" action="" class="form">

        <div class="form-group">
            <label class="col-md-4 control-label" for="gezinsnummer">[[%Gezinsnummer]]</label>
            <div class="form__fields-field cell large-6">
                <input id="gezinsnummer" name="gezinsnummer" type="number" class="form-control input-md" maxlength="4" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="naamouder">[[%Naam]] [[%ouder]]/[[%verzorger]]</label>
            <div class="form__fields-field cell large-6">
                <input id="naamouder" name="naamouder" type="text" class="form-control input-md" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="emailouder">[[%E-mail]] [[%ouder]]/[[%verzorger]]</label>
            <div class="form__fields-field cell large-6">
                <input id="emailouder" name="emailouder" type="email" class="form-control input-md" required>
            </div>
        </div>

        <label for="kinderen">[[%Selecteer aantal kinderen]]:</label>
        <select id="kinderen" onchange="dropDownMenu();" required>
            <option value="-1" disabled selected>Kies hier hoeveel kinderen je wilt aanmelden.</option>
            <option value="1">Ik wil 1 kind aanmelden</option>
            <option value="2">Ik wil 2 kinderen aanmelden</option>
            <option value="3">Ik wil 3 kinderen aanmelden</option>
            <option value="4">Ik wil 4 kinderen aanmelden</option>
        </select>

        <br>

        <div class="hidden" id="kind1">

            <br>

            <h4>[[%Kind 1]]</h4>

            <div class="form-group">
                <label for="kind1voornaam">[[%Voornaam]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="text" id="kind1voornaam" name="kind1voornaam" required>
                </div>
            </div>

            <div class="form-group">
                <label for="kind1achternaam">[[%Achternaam]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="text" id="kind1achternaam" name="kind1achternaam" required>
                </div>
            </div>

            <div class="form-group">
                <label for="kind1geboortedatum">[[%Geboortedatum]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="date" id="kind1geboortedatum" name="kind1geboortedatum" required>
                </div>
            </div>

            <div class="form-group">
                <label for="kind1school">[[%School]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind1school" name="kind1school" required>
                        <option value="">Kies school</option>
                        <option value="Alma College">Alma College</option>
                        <option value="Canisius Almelo mavo">Canisius Almelo mavo</option>
                        <option value="Canisius Almelo havo, vwo">Canisius Almelo havo, vwo</option>
                        <option value="Het Erasmus praktijkonderwijs">Het Erasmus praktijkonderwijs</option>
                        <option value="Het Erasmus mavo">Het Erasmus mavo</option>
                        <option value="Het Erasmus havo, vwo">Het Erasmus havo, vwo</option>
                        <option value="Het Erasmus isk">Het Erasmus isk</option>
                        <option value="Het Noordik Lyceum mavo">Het Noordik Lyceum mavo</option>
                        <option value="Het Noordik Lyceum havo, vwo">Het Noordik Lyceum havo, vwo</option>
                        <option value="Het Pius X College mavo">Het Pius X College mavo</option>
                        <option value="Het Pius X College havo, vwo">Het Pius X College havo, vwo</option>
                        <option value="Zone.college">Zone.college</option>
                        <option value="Omnis College">Omnis College</option>
                        <option value="VSO De Veenlanden">VSO De Veenlanden</option>
                        <option value="VSO De Brug">VSO De Brug</option>
                        <option value="VSO Het Mozaiek">VSO Het Mozaiek</option>
                        <option value="andere">Anders, namelijk:</option>
                    </select>
                </div>
            </div>

            <div class="form-group margin-top-bottom margin-left" id="andere-school-textarea1" style="display: none;">
                <label for="andere-school1">[[%Andere school]]:</label>
                <input type="text" id="andere-school1" name="andere-school1">
            </div>

            <div class="form-group">
                <label for="kind1klas">[[%In welke klas zit het kind op dit moment?]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind1klas" name="kind1klas" required>
                        <option value="">Selecteer een klas</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="kind1klaszomervakantie">[[%Naar welke klas gaat het kind waarschijnlijk na de zomervakantie?]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind1klaszomervakantie" name="kind1klaszomervakantie" required>
                        <option value="">Selecteer een klas</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="opmerkingen1">[[%Opmerkingen]]</label>
                <div class="form__fields-field cell large-6">
                    <textarea id="opmerkingen1" name="opmerkingen1"></textarea>
                </div>
            </div>
        </div>

        <div class="hidden" id="kind2">

            <br>

            <h4>[[%Kind 2]]</h4>

            <div class="form-group">
                <label for="kind2voornaam">[[%Voornaam]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="text" id="kind2voornaam" name="kind2voornaam">
                </div>
            </div>

            <div class="form-group">
                <label for="kind2achternaam">[[%Achternaam]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="text" id="kind2achternaam" name="kind2achternaam">
                </div>
            </div>

            <div class="form-group">
                <label for="kind2geboortedatum">[[%Geboortedatum]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="date" id="kind2geboortedatum" name="kind2geboortedatum">
                </div>
            </div>

            <div class="form-group">
                <label for="kind2school">[[%School]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind2school" name="kind2school">
                        <option value="">Kies school</option>
                        <option value="Alma College">Alma College</option>
                        <option value="Canisius Almelo mavo">Canisius Almelo mavo</option>
                        <option value="Canisius Almelo havo, vwo">Canisius Almelo havo, vwo</option>
                        <option value="Het Erasmus praktijkonderwijs">Het Erasmus praktijkonderwijs</option>
                        <option value="Het Erasmus mavo">Het Erasmus mavo</option>
                        <option value="Het Erasmus havo, vwo">Het Erasmus havo, vwo</option>
                        <option value="Het Erasmus isk">Het Erasmus isk</option>
                        <option value="Het Noordik Lyceum mavo">Het Noordik Lyceum mavo</option>
                        <option value="Het Noordik Lyceum havo, vwo">Het Noordik Lyceum havo, vwo</option>
                        <option value="Het Pius X College mavo">Het Pius X College mavo</option>
                        <option value="Het Pius X College havo, vwo">Het Pius X College havo, vwo</option>
                        <option value="Zone.college">Zone.college</option>
                        <option value="Omnis College">Omnis College</option>
                        <option value="VSO De Veenlanden">VSO De Veenlanden</option>
                        <option value="VSO De Brug">VSO De Brug</option>
                        <option value="VSO Het Mozaiek">VSO Het Mozaiek</option>
                        <option value="andere">Anders, namelijk:</option>
                    </select>
                </div>
            </div>

            <div class="form-group margin-top-bottom margin-left" id="andere-school-textarea2" style="display: none;">
                <label for="andere-school2">[[%Andere school]]:</label>
                <input type="text" id="andere-school2" name="andere-school2">
            </div>

            <div class="form-group">
                <label for="kind2klas">[[%In welke klas zit het kind op dit moment?]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind2klas" name="kind2klas">
                        <option value="">Selecteer een klas</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="kind2klaszomervakantie">[[%Naar welke klas gaat het kind waarschijnlijk na de zomervakantie?]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind2klaszomervakantie" name="kind2klaszomervakantie">
                        <option value="">Selecteer een klas</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="opmerkingen2">[[%Opmerkingen]]</label>
                <div class="form__fields-field cell large-6">
                    <textarea id="opmerkingen2" name="opmerkingen2"></textarea>
                </div>
            </div>
        </div>


        <div class="hidden" id="kind3">

            <br>

            <h4>[[%Kind 3]]</h4>

            <div class="form-group">
                <label for="kind3voornaam">[[%Voornaam]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="text" id="kind3voornaam" name="kind3voornaam">
                </div>
            </div>

            <div class="form-group">
                <label for="kind3achternaam">[[%Achternaam]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="text" id="kind3achternaam" name="kind3achternaam">
                </div>
            </div>

            <div class="form-group">
                <label for="kind3geboortedatum">[[%Geboortedatum]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="date" id="kind3geboortedatum" name="kind3geboortedatum">
                </div>
            </div>

            <div class="form-group">
                <label for="kind3school">[[%School]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind3school" name="kind3school">
                        <option value="">Kies school</option>
                        <option value="Alma College">Alma College</option>
                        <option value="Canisius Almelo mavo">Canisius Almelo mavo</option>
                        <option value="Canisius Almelo havo, vwo">Canisius Almelo havo, vwo</option>
                        <option value="Het Erasmus praktijkonderwijs">Het Erasmus praktijkonderwijs</option>
                        <option value="Het Erasmus mavo">Het Erasmus mavo</option>
                        <option value="Het Erasmus havo, vwo">Het Erasmus havo, vwo</option>
                        <option value="Het Erasmus isk">Het Erasmus isk</option>
                        <option value="Het Noordik Lyceum mavo">Het Noordik Lyceum mavo</option>
                        <option value="Het Noordik Lyceum havo, vwo">Het Noordik Lyceum havo, vwo</option>
                        <option value="Het Pius X College mavo">Het Pius X College mavo</option>
                        <option value="Het Pius X College havo, vwo">Het Pius X College havo, vwo</option>
                        <option value="Zone.college">Zone.college</option>
                        <option value="Omnis College">Omnis College</option>
                        <option value="VSO De Veenlanden">VSO De Veenlanden</option>
                        <option value="VSO De Brug">VSO De Brug</option>
                        <option value="VSO Het Mozaiek">VSO Het Mozaiek</option>
                        <option value="andere">Anders, namelijk:</option>
                    </select>
                </div>
            </div>

            <div class="form-group margin-top-bottom margin-left" id="andere-school-textarea3" style="display: none;">
                <label for="andere-school3">Andere school:</label>
                <input type="text" id="andere-school3" name="andere-school3">
            </div>

            <div class="form-group">
                <label for="kind3klas">[[%In welke klas zit het kind op dit moment?]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind3klas" name="kind3klas">
                        <option value="">Selecteer een klas</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="kind3klaszomervakantie">[[%Naar welke klas gaat het kind waarschijnlijk na de zomervakantie?]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind3klaszomervakantie" name="kind3klaszomervakantie">
                        <option value="">Selecteer een klas</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="opmerkingen3">Opmerkingen</label>
                <div class="form__fields-field cell large-6">
                    <textarea id="opmerkingen3" name="opmerkingen3"></textarea>
                </div>
            </div>
        </div>

        <div class="hidden" id="kind4">

            <br>

            <h4>Kind 4</h4>

            <div class="form-group">
                <label for="kind4voornaam">[[%Voornaam]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="text" id="kind4voornaam" name="kind4voornaam">
                </div>
            </div>

            <div class="form-group">
                <label for="kind4achternaam">[[%Achternaam]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="text" id="kind4achternaam" name="kind4achternaam">
                </div>
            </div>

            <div class="form-group">
                <label for="kind4geboortedatum">[[%Geboortedatum]]</label>
                <div class="form__fields-field cell large-6">
                    <input type="date" id="kind4geboortedatum" name="kind4geboortedatum">
                </div>
            </div>

            <div class="form-group">
                <label for="kind4school">[[%School]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind4school" name="kind4school">
                        <option value="">Kies school</option>
                        <option value="Alma College">Alma College</option>
                        <option value="Canisius Almelo mavo">Canisius Almelo mavo</option>
                        <option value="Canisius Almelo havo, vwo">Canisius Almelo havo, vwo</option>
                        <option value="Het Erasmus praktijkonderwijs">Het Erasmus praktijkonderwijs</option>
                        <option value="Het Erasmus mavo">Het Erasmus mavo</option>
                        <option value="Het Erasmus havo, vwo">Het Erasmus havo, vwo</option>
                        <option value="Het Erasmus isk">Het Erasmus isk</option>
                        <option value="Het Noordik Lyceum mavo">Het Noordik Lyceum mavo</option>
                        <option value="Het Noordik Lyceum havo, vwo">Het Noordik Lyceum havo, vwo</option>
                        <option value="Het Pius X College mavo">Het Pius X College mavo</option>
                        <option value="Het Pius X College havo, vwo">Het Pius X College havo, vwo</option>
                        <option value="Zone.college">Zone.college</option>
                        <option value="Omnis College">Omnis College</option>
                        <option value="VSO De Veenlanden">VSO De Veenlanden</option>
                        <option value="VSO De Brug">VSO De Brug</option>
                        <option value="VSO Het Mozaiek">VSO Het Mozaiek</option>
                        <option value="andere">Anders, namelijk:</option>
                    </select>
                </div>
            </div>

            <div class="form-group margin-top-bottom margin-left" id="andere-school-textarea4" style="display: none;">
                <label for="andere-school4">[[%Andere school]]:</label>
                <input type="text" id="andere-school4" name="andere-school4">
            </div>

            <div class="form-group">
                <label for="kind4klas">[[%In welke klas zit het kind op dit moment?]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind4klas" name="kind4klas">
                        <option value="">Selecteer een klas</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="kind4klaszomervakantie">[[%Naar welke klas gaat het kind waarschijnlijk na de zomervakantie?]]</label>
                <div class="form__fields-field cell large-6">
                    <select id="kind4klaszomervakantie" name="kind4klaszomervakantie">
                        <option value="">Selecteer een klas</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="opmerkingen4">[[%Opmerkingen]]</label>
                <div class="form__fields-field cell large-6">
                    <textarea id="opmerkingen4" name="opmerkingen4"></textarea>
                </div>
            </div>
        </div>

        <div style="margin-top: 3%;margin-bottom: 3%">
            <b>[[%form_check]].</b>
        </div>

        <div id="phoneNumberWarning" class="hidden margin-top-bottom">
            <h4 class="color-warning">[[%Je telefoonnummer klopt niet!]]</h4>
        </div>

        <div id="postalCodeWarning" class="hidden margin-top-bottom">
            <h4 class="color-warning">[[%Je postcode klopt niet!]]</h4>
        </div>

        <button class="button primary yellow" id="sendButton">[[%Verzenden]]</button>

        <input type="hidden" name="form-schoolpas" value="Send" />
        <input type="hidden" name="previous" id="previous" value="[[#SERVER.HTTP_REFERER]]" />
        <input type="hidden" name="ghost" value="" />
        <input type="hidden" name="csrf" value="[[!csrf.generate]]" />
    </form>
</div>

<script>
    const sendButton = document.getElementById("sendButton");
    const dropdown = document.getElementById("kinderen");

    const kind1 = document.getElementById("kind1");
    const kind2 = document.getElementById("kind2");
    const kind3 = document.getElementById("kind3");
    const kind4 = document.getElementById("kind4");

    var select1 = document.getElementById("kind1school");
    var select2 = document.getElementById("kind2school");
    var select3 = document.getElementById("kind3school");
    var select4 = document.getElementById("kind4school");
    var andereInput1 = document.getElementById("andere-school-textarea1");
    var andereInput2 = document.getElementById("andere-school-textarea2");
    var andereInput3 = document.getElementById("andere-school-textarea3");
    var andereInput4 = document.getElementById("andere-school-textarea4");

    var kindinfo2 = [
        document.getElementById("kind2voornaam"),
        document.getElementById("kind2achternaam"),
        document.getElementById("kind2geboortedatum"),
        document.getElementById("kind2school"),
        document.getElementById("kind2klas"),
        document.getElementById("kind2klaszomervakantie")
    ];

    var kindinfo3 = [
        document.getElementById("kind3voornaam"),
        document.getElementById("kind3achternaam"),
        document.getElementById("kind3geboortedatum"),
        document.getElementById("kind3school"),
        document.getElementById("kind3klas"),
        document.getElementById("kind3klaszomervakantie")
    ];

    var kindinfo4 = [
        document.getElementById("kind4voornaam"),
        document.getElementById("kind4achternaam"),
        document.getElementById("kind4geboortedatum"),
        document.getElementById("kind4school"),
        document.getElementById("kind4klas"),
        document.getElementById("kind4klaszomervakantie")
    ];

    select1.addEventListener("change", function() {
        if (select1.value === "andere") {
            andereInput1.style.display = "block";
        } else {
            andereInput1.style.display = "none";
        }
    });

    select2.addEventListener("change", function() {
        if (select2.value === "andere") {
            andereInput2.style.display = "block";
        } else {
            andereInput2.style.display = "none";
        }
    });

    select3.addEventListener("change", function() {
        if (select3.value === "andere") {
            andereInput3.style.display = "block";
        } else {
            andereInput3.style.display = "none";
        }
    });

    select4.addEventListener("change", function() {
        if (select4.value === "andere") {
            andereInput4.style.display = "block";
        } else {
            andereInput4.style.display = "none";
        }
    });

    function dropDownMenu() {
        let value = dropdown.value;

        switch (value) {
            case '1':
                kind1.classList.remove("hidden");
                kind2.classList.add("hidden");
                kind3.classList.add("hidden");
                kind4.classList.add("hidden");
                kindinfo2.forEach(function(element) {
                    element.removeAttribute("required", "required");
                });
                kindinfo3.forEach(function(element) {
                    element.removeAttribute("required", "required");
                });
                kindinfo4.forEach(function(element) {
                    element.removeAttribute("required", "required");
                });
                break;
            case '2':
                kind1.classList.remove("hidden");
                kind2.classList.remove("hidden");
                kind3.classList.add("hidden");
                kind4.classList.add("hidden");
                kindinfo2.forEach(function(element) {
                    element.setAttribute("required", "required");
                });
                kindinfo3.forEach(function(element) {
                    element.removeAttribute("required", "required");
                });
                kindinfo4.forEach(function(element) {
                    element.removeAttribute("required", "required");
                });
                break;
            case '3':
                kind1.classList.remove("hidden");
                kind2.classList.remove("hidden");
                kind3.classList.remove("hidden");
                kind4.classList.add("hidden");
                kindinfo2.forEach(function(element) {
                    element.setAttribute("required", "required");
                });
                kindinfo3.forEach(function(element) {
                    element.setAttribute("required", "required");
                });
                kindinfo4.forEach(function(element) {
                    element.removeAttribute("required", "required");
                });
                break;
            case '4':
                kind1.classList.remove("hidden");
                kind2.classList.remove("hidden");
                kind3.classList.remove("hidden");
                kind4.classList.remove("hidden");
                kindinfo2.forEach(function(element) {
                    element.setAttribute("required", "required");
                });
                kindinfo3.forEach(function(element) {
                    element.setAttribute("required", "required");
                });
                kindinfo4.forEach(function(element) {
                    element.setAttribute("required", "required");
                });
                break;
            default:
                kind1.classList.add("hidden");
                kind2.classList.add("hidden");
                kind3.classList.add("hidden");
                kind4.classList.add("hidden");
                break;
        }
    }
</script>