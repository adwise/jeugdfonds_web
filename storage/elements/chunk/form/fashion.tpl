{$_modx->runSnippet('!FormIt@fashion', [
'redirectTo' => $block.redirect
])}

<div class="alert alert-error">[[!+fi.validation_error_message]]  <pre>[[!+fi.error_message]]</pre></div>
<form method="post" action="" class="validate fashion-form [[!+fi.validation_error_message:notempty=`error`]]" accept-charset="utf-8" enctype="application/x-www-form-urlencoded" id="fashion-form">
    <div class="form-group">
        <label class="col-md-4 control-label" for="gezinsnummer">[[%Gezinsnummer]]</label>
        <div class="form__fields-field cell large-6">
            <input id="gezinsnummer" name="gezinsnummer" type="number" class="form-control input-md" maxlength="4" required>
        </div>
    </div>

    <div class="form-group">
        <label for="form-name" class="col-md-4 control-label">[[%Voornaam ouder/verzorger]]</label>
        <div class="form__fields-field cell large-6">
            <input type="text" name="voornaamouder" id="form-firstname" required>
        </div>
    </div>

    <div class="form-group">
        <label for="form-name" class="col-md-4 control-label">[[%Achternaam ouder/verzorger]]</label>
        <div class="form__fields-field cell large-6">
            <input type="text" name="achternaamouder" id="form-lastname" required>
        </div>
    </div>

    <div class="form-group">
        <label for="form-email" class="col-md-4 control-label">[[%E-mailadres]]</label>
        <div class="form__fields-field cell large-6">
            <input type="text" name="email" id="form-email" required>
        </div>
    </div>

    <div class="form-group">
        <label for="phone">[[%Telefoonnummer]]</label>
        <div class="form__fields-field cell large-6">
            <input type="tel" id="phone" name="telefoonnummer" onBlur="phoneNumberCheck(document.getElementById('phone').value)" placeholder="Vul hier je telefoonnummer in met minimaal 10 cijfers" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label" for="straatnaam">[[%Straatnaam]]</label>
        <div class="form__fields-field cell large-6">
            <input id="straatnaam" name="straatnaam" type="text" class="form-control input-md" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label" for="huisnummer">[[%Huisnummer]] [[%en]] [[%toevoeging]]</label>
        <div class="form__fields-field cell large-6">
            <input id="huisnummer" name="huisnummer" type="text" class="form-control input-md" required>
        </div>
    </div>

    <div class="form-group">
        <label for="postcode" class="col-md-4 control-label">[[%Postcode]]</label>
        <div class="form__fields-field cell large-6">
            <input id="postcode" name="postcode" type="text" class="form-control input-md" onBlur="postalCodeCheck(document.getElementById('postcode').value)" placeholder="Postcode format: 1111AB of 1111 AB" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label" for="plaats">[[%Plaats]]</label>
        <div class="form__fields-field cell large-6">
            <input id="plaats" name="plaats" type="text" class="form-control input-md" required>
        </div>
    </div>

    <label for="kinderen">[[%Selecteer aantal kinderen]]:</label>
    <select id="kinderen" required>
        <option value="-1" disabled selected>[[%Kies hier hoeveel kinderen je wilt aanmelden]].</option>
        <option value="1">[[%Ik wil 1 kind aanmelden]]</option>
        <option value="2">[[%Ik wil 2 kinderen aanmelden]]</option>
        <option value="3">[[%Ik wil 3 kinderen aanmelden]]</option>
        <option value="4">[[%Ik wil 4 kinderen aanmelden]]</option>
    </select>

    <br>

    <div class="hidden" id="kind1">

        <br>

        <h4>[[%Kind 1]]</h4>

        <div class="form-group">
            <label for="geslacht1">[[%Geslacht]]:</label>
            <select name="geslacht1" id="geslacht1" required>
                <option value="">[[%Kies geslacht]]</option>
                <option value="Jongen">[[%Jongen]]</option>
                <option value="Meisje">[[%Meisje]]</option>
                <option value="Anders gekozen kind 1">[[%Anders, namelijk]]:</option>
            </select>
        </div>

        <div class="form-group margin-top-bottom margin-left" id="andere-geslacht-textarea1" style="display: none;">
            <label for="andere-geslacht1">[[%Geslacht]]:</label>
            <input type="text" id="andere-geslacht1" name="andere-geslacht1" value="[[+andere-geslacht1]]">
        </div>

        <br>

        <div class="form-group">
            <label for="kind1voornaam">[[%Voornaam]]</label>
            <div class="form__fields-field cell large-6">
                <input type="text" id="kind1voornaam" name="kind1voornaam" required>
            </div>
        </div>

        <div class="form-group">
            <label for="kind1achternaam">[[%Achternaam]]</label>
            <div class="form__fields-field cell large-6">
                <input type="text" id="kind1achternaam" name="kind1achternaam" required>
            </div>
        </div>

        <div class="form-group">
            <label for="kind1geboortedatum">[[%Geboortedatum]]</label>
            <div class="form__fields-field cell large-6">
                <input type="date" id="kind1geboortedatum" name="kind1geboortedatum" required>
            </div>
        </div>
    </div>

    <div class="hidden" id="kind2">

        <br>

        <h4>[[%Kind 2]]</h4>

        <div class="form-group">
            <label for="geslacht2">[[%Geslacht]]:</label>
            <select name="geslacht2" id="geslacht2">
                <option value="">[[%Kies geslacht]]</option>
                <option value="Jongen">[[%Jongen]]</option>
                <option value="Meisje">[[%Meisje]]</option>
                <option value="Anders gekozen kind 2">[[%Anders, namelijk]]:</option>
            </select>
        </div>

        <br>

        <div class="form-group margin-top-bottom margin-left" id="andere-geslacht-textarea2" style="display: none;">
            <label for="andere-geslacht2">[[%Geslacht]]:</label>
            <input type="text" id="andere-geslacht2" name="andere-geslacht2" value="[[+andere-geslacht2]]">
        </div>

        <div class="form-group">
            <label for="kind2voornaam">[[%Voornaam]]</label>
            <div class="form__fields-field cell large-6">
                <input type="text" id="kind2voornaam" name="kind2voornaam">
            </div>
        </div>

        <div class="form-group">
            <label for="kind2achternaam">[[%Achternaam]]</label>
            <div class="form__fields-field cell large-6">
                <input type="text" id="kind2achternaam" name="kind2achternaam">
            </div>
        </div>

        <div class="form-group">
            <label for="kind2geboortedatum">[[%Geboortedatum]]</label>
            <div class="form__fields-field cell large-6">
                <input type="date" id="kind2geboortedatum" name="kind2geboortedatum">
            </div>
        </div>
    </div>

    <div class="hidden" id="kind3">

        <br>

        <h4>[[%Kind 3]]</h4>

        <div class="form-group">
            <label for="geslacht3">[[%Geslacht]]:</label>
            <select name="geslacht3" id="geslacht3">
                <option value="">[[%Kies geslacht]]</option>
                <option value="Jongen">[[%Jongen]]</option>
                <option value="Meisje">[[%Meisje]]</option>
                <option value="Anders gekozen kind 3">[[%Anders, namelijk]]:</option>
            </select>
        </div>

        <div class="form-group margin-top-bottom margin-left" id="andere-geslacht-textarea3" style="display: none;">
            <label for="andere-geslacht3">[[%Geslacht]]:</label>
            <input type="text" id="andere-geslacht3" name="andere-geslacht3" value="[[+andere-geslacht3]]">
        </div>

        <div class="form-group">
            <label for="kind3voornaam">[[%Voornaam]]</label>
            <div class="form__fields-field cell large-6">
                <input type="text" id="kind3voornaam" name="kind3voornaam">
            </div>
        </div>

        <div class="form-group">
            <label for="kind3achternaam">[[%Achternaam]]</label>
            <div class="form__fields-field cell large-6">
                <input type="text" id="kind3achternaam" name="kind3achternaam">
            </div>
        </div>

        <div class="form-group">
            <label for="kind3geboortedatum">[[%Geboortedatum]]</label>
            <div class="form__fields-field cell large-6">
                <input type="date" id="kind3geboortedatum" name="kind3geboortedatum">
            </div>
        </div>
    </div>

    <div class="hidden" id="kind4">

        <br>

        <h4>[[%Kind 4]]</h4>

        <div class="form-group">
            <label for="geslacht4">[[%Geslacht]]:</label>
            <select name="geslacht4" id="geslacht4">
                <option value="">Kies geslacht</option>
                <option value="Jongen">Jongen</option>
                <option value="Meisje">Meisje</option>
                <option value="Anders gekozen kind 4">Anders, namelijk:</option>
            </select>
        </div>

        <div class="form-group margin-top-bottom margin-left" id="andere-geslacht-textarea4" style="display: none;">
            <label for="andere-geslacht4">[[%Geslacht]]:</label>
            <input type="text" id="andere-geslacht4" name="andere-geslacht4" value="[[+andere-geslacht4]]">
        </div>

        <div class="form-group">
            <label for="kind4voornaam">[[%Voornaam]]</label>
            <div class="form__fields-field cell large-6">
                <input type="text" id="kind4voornaam" name="kind4voornaam">
            </div>
        </div>

        <div class="form-group">
            <label for="kind4achternaam">[[%Achternaam]]</label>
            <div class="form__fields-field cell large-6">
                <input type="text" id="kind4achternaam" name="kind4achternaam">
            </div>
        </div>

        <div class="form-group">
            <label for="kind4geboortedatum">[[%Geboortedatum]]</label>
            <div class="form__fields-field cell large-6">
                <input type="date" id="kind4geboortedatum" name="kind4geboortedatum">
            </div>
        </div>
    </div>

    <div style="margin-top: 3%;margin-bottom: 3%">
        <b>[[%form_check]].</b>
    </div>

    <div id="phoneNumberWarning" class="hidden margin-top-bottom">
        <h4 class="color-warning">[[%Je telefoonnummer klopt niet!]]</h4>
    </div>

    <div id="postalCodeWarning" class="hidden margin-top-bottom">
        <h4 class="color-warning">[[%Je postcode klopt niet!]]</h4>
    </div>

    [[!recaptchav3_render]]
    [[!+fi.error.recaptchav3_error]]

    <button class="button primary yellow" id="sendButton">Verzenden</button>

    <input type="hidden" name="form-fashion" value="Send" />
    <input type="hidden" name="previous" id="previous" value="[[#SERVER.HTTP_REFERER]]" />
    <input type="hidden" name="ghost" value="" />
    <input type="hidden" name="csrf" value="[[!csrf.generate]]" />
    <input type="hidden" name="referral URL" value="[[~[[*id]]? &scheme=`full`]]"/>
    <input type="hidden" name="referral ID" value="[[*id]]"/>
</form>