<div id="cookie-consent" class="fade-in callout hide" role="alert">
    <div class="grid-container grid-padding-x">
        <div class="grid-x grid-padding-x">
            <div class="small-12 cell">
                <div class="cookie-title">{$_modx->config['cookie_consent-title']}</div>
            </div>
        </div>
        <div class="grid-x grid-padding-x">
            <div class="small-12 medium-8 cell">
                <p>{$_modx->config['cookie_consent-text']} <a href="{$_modx->config['cookie_consent-href']|inOrExternalUrl}" title="{$_modx->config['cookie_consent-link-text']}">{$_modx->config['cookie_consent-link-text']}</a></p>
            </div>
            <div class="small-12 medium-4 cell text-right">
                <button type="button" class="button yellow" data-dismiss="alert">Sluiten</button>
            </div>
        </div>
    </div>
</div>