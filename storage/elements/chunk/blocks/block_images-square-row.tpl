<div class="cell">
    <figure class="{$block.show_border == 'show' ? 'show-border' : ''}">
        {'ImagePlus' | snippet : ['type' => 'tpl', 'tpl' => 'block_images-row-image', 'value' => $item.image]}
    </figure>
</div>