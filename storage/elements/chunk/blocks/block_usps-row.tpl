<div class="usps__cards-card cell small-12 large-4">
    <div class="grid-x">
        <div class="cell small-2 large-12">
            <figure>
                {if $item.image == ''}
                    <img src="/static/default/media/images/templating/svg/{$item.icon}.svg" alt="{$item.title}">
                {else}
                    <img class="image" src="{'ImagePlus' | snippet : ['value' => $item.image, 'options' => 'w=150&zc=1']}" alt="{$item.title}">
                {/if}
            </figure>
        </div>
        <div class="cell small-9 large-12 text {$item.image == '' ? 'margin' : '' }">
            <h3 class="heading-s">{$item.title}</h3>
            {if $item.text != ''}<p>{$item.text}</p>{/if}
        </div>
    </div>
</div>