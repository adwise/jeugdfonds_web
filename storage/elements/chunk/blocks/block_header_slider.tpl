<section class="block_header_slider">
    <div class="grid-container full">
        <div class="slider">

            {set $slides = json_decode($block.slides, true)}

            {foreach $slides as $idx => $slide}
                {include ('file:chunk/blocks/block_header_slider_row.tpl') length=$slides|length slide=$slide}
            {/foreach}

        </div>
    </div>
</section>
