<section class="block_images">
	<div class="grid-container">

		{if $block.title != ''}
		<div class="grid-x">
			<header>
				<h2 class="heading-m">{$block.title}</h2>
			</header>
		</div>
		{/if}

		<div class="grid-x grid-margin-x grid-margin-y {$block.up_small} {$block.up_medium} {$block.up_large}">

			{foreach json_decode($block.items, true) as $idx => $item}
				{include ('file:chunk/blocks/block_images-square-row.tpl') item=$item}
			{/foreach}

		</div>
	</div>
</section>