<section class="block_cta">
    <div class="grid-container">
        <div class="wrapper grid-x align-middle">
            <img class="triangle" src="/static/default/media/svg/triangle.svg" alt="triangle">
            <img class="oval" src="/static/default/media/svg/oval.svg" alt="oval">

            {if $block.image != ''}
                <div class="image cell small-12 large-4 show-for-large">
                    <figure>
                        <img src="{'ImagePlus' | snippet : ['value' => $block.image, 'options' => 'w=600&zc=1']}" alt="{$block.title}">
                    </figure>
                </div>
            {/if}

            <div class="{$block.image != '' ? 'small-12 large-4 large-offset-1' : 'cell small-12'}">
                <h2 class="heading-m">{$block.title}</h2>
                <p>{$block.text}</p>
                {if $block.cta_options == ''}<div class="mt"><a href="{$block.link_href|url}" class="button primary purple">{$block.link_caption}</a></div>{/if}

                {if $block.cta_options != ''}

                    <form method="post" action="{$block.link_href|url}">
                        <div class="small-12 cell input-group radio">
                            <fieldset>

                                {set $options = json_decode($block.cta_options, true)}
                                {foreach $options as $idx => $option}

                                    <input type="checkbox" name="{$idx}" value="{$option.title}" id="{$idx}" class="group-check"/>
                                    <label for="{$idx}">{$option.title}</label>

                                {/foreach}

                                <button class="button primary purple sendform">{$block.link_caption}</button>
                            </fieldset>
                        </div>
                    </form>
                {/if}
            </div>

        </div>
    </div>
</section>