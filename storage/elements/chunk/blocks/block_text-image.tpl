<section class="block_text-image">
    <div class="grid-container">
        <div class="grid-x align-middle">
            <div class="text {$block.image_position == "left"? "cell large-6 large-offset-1 large-order-2" : "cell large-6"}">
                <header>
                    <img class="decoration" src="/static/default/media/svg/elements.svg" alt="decoration">
                    <h2 class="heading-m">{$block.title}</h2>
                </header>
                {$block.text}
                {if $block.link_text != ''}<div class="mt"><a class="button secondary white" href="{$block.link_href|url}">{$block.link_text}</a></div>{/if}
            </div>
            <div class="image {$block.image_position == "left"? "cell large-5 large-order-1" : "cell large-5 large-offset-1"}">
                <img class="image-triangle" src="/static/default/media/svg/triangle.svg" alt="triangle">
                <img class="image-oval" src="/static/default/media/svg/oval.svg" alt="oval">
                <img class="image-rectangle" src="/static/default/media/svg/rectangle.svg" alt="rectangle">
                <figure>
                    <img src="{'ImagePlus' | snippet : ['value' => $block.image, 'options' => 'w=1920&zc=1']}" alt="{$block.title|striptags}">
                </figure>
            </div>
        </div>
    </div>
</section>