{set $babel = $_modx->resource.id|resource:'babelLanguageLinks'}

{if $babel}
    {set $ctx_explode = $babel|split:';'}
    {foreach $ctx_explode as $ctx_res}
        {set $res_explode = $ctx_res|split:':'}
        {set $contextKey = $res_explode.0 }
        {set $resourceId = intval($res_explode.1) }
        {if $resourceId}
            <link rel="{$_modx->context.key == $contextKey? 'canonical' : 'alternate'}" hreflang="{$contextKey}" href="{$resourceId|url}" />
        {/if}
    {/foreach}
{/if}