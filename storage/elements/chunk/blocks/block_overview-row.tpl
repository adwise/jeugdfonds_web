<div class="cell small-12 large-6 xxlarge-{$columns}">
    <a {if $clickable == 1}href="{$link}" title="{$longetitle? : $pagetitle}" {else}class="no-link"{/if} >
        {if $show_image == 1}
            <div class="block_overview-image">
                <figure>
                    {'ImagePlus' | snippet : ['tvname' => 'media_overview-image', 'docid' => $id, 'options' => 'w=640&q=80', 'type' => 'tpl']}
                </figure>
            </div>
        {/if}

        <div class="block_overview-content">
            {$show_title == 1 ? '<h3 class="heading-s">[[+pagetitle]]</h3>': ''}
            {$show_date == 1 ? '<p class="subheading">[[+publishedon:date=`%d %B %Y`]]</p>': ''}
            {$show_sub == 1 ? '<h4 class="subheading">[[+longtitle]]</h4>': ''}
            {$show_introtext == 1 ? '<p>[[+introtext:ellipsis=`175`]]</p>':''}
            {$show_cta == 1 ? '<a href="[[+link]]" class="button tertiary" title="[[+pagetitle]]">Lees meer</a>':''}
        </div>
    </a>
</div>