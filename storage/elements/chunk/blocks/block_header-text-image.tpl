<section class="block_header-text-image">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12 medium-10 xlarge-6">

                {var $resource = $_modx->getResource($block.featured_href)}
                {if $block.featured_href != ''}<div class="featured"><span>{$block.featured_tag}</span><a href="{$block.featured_href|url}">{$resource.pagetitle}</a></div>{/if}

                <header>
                    <h1 class="heading-l">{$block.title}</h1>
                </header>
                {$block.text}
                {if $block.link_text != ''}<div class="mt"><a class="button primary yellow" href="{$block.link_href|url}">{$block.link_text}</a></div>{/if}
            </div>
            <div class="image {$block.image_position == "left"? "cell large-5 large-order-1" : "cell large-5 large-offset-1"}">

                <img class="image-triangle" src="/static/default/media/svg/triangle.svg" alt="triangle">
                <img class="image-oval" src="/static/default/media/svg/oval.svg" alt="oval">
                <img class="image-rectangle" src="/static/default/media/svg/rectangle.svg" alt="rectangle">
                <img class="image-zon" src="/static/default/media/svg/zon.svg" alt="zon">

                <figure>
                    <img src="{'ImagePlus' | snippet : ['value' => $block.image, 'options' => 'w=1920&zc=1']}" alt="{$block.title}">
                </figure>
            </div>
        </div>
    </div>
</section>