<section class="block_intro">
    <div class="grid-container">
        <div class="grid-x align-center">
            <div class="cell small-12 medium-10 text-center">
                {if $block.title != ""}<h2 class="heading-l">{$block.title}</h2>{/if}
                <p>{$block.text}</p>
                {if $block.link_caption != ""}<a href="{$block.link_href|url}" class="btn">{$block.link_caption}</a>{/if}
            </div>
        </div>
    </div>
</section>