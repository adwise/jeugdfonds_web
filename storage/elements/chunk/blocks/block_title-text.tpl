<section class="block_title-text">
    <div class="grid-container">
        <article>
            <div class="grid-x align-center">
                <div class="cell small-12 medium-10 large-8">
                    <header>
                        <img class="decoration" src="/static/default/media/svg/elements.svg" alt="decoration">
                        <h2 class="heading-m">{$block.title}</h2>
                    </header>
                </div>
                <div class="cell small-12 medium-10 large-8">
                    {$block.text}
                </div>
            </div>
        </article>
    </div>
</section>
