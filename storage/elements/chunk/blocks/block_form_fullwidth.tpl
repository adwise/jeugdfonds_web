<section class="block_form_fullwidth">
    <div class="grid-container">
        <div class="form grid-x align-center">
            {if $block.title or $block.text}
                <div class="cell small-12 large-8 text-center">
                    <header>
                        {if $block.title}
                            <h2 class="heading-m">{$block.title}</h2>
                        {/if}
                        {if $block.text}{$block.text}{/if}
                    </header>
                </div>
            {/if}
            <div class="form__fields cell {$block.title_form == '' ? 'no-title' : ''}">
                {include ('file:chunk/form/' ~ $block.form ~ '.tpl') redirect=$block.redirect}
            </div>
        </div>
    </div>
</section>