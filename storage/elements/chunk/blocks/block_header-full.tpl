<section class="block_header-full">
    <div class="grid-container full">
        <div class="grid-x align-center">
            <div class="cell small-12 medium-10 text-center">
                {if $block.title != ""}<h1 class="heading-l">{$block.title}</h1>{/if}
                {$block.text}
            </div>
        </div>
    </div>
    <div class="grid-container no-padding">
        <div class="grid-x align-center">
            <div class="image">
                <img class="image-triangle" src="/static/default/media/svg/triangle.svg" alt="triangle">
                <img class="image-oval" src="/static/default/media/svg/oval.svg" alt="oval">
                <img class="image-rectangle" src="/static/default/media/svg/rectangle.svg" alt="rectangle">
                <img class="image-zon" src="/static/default/media/svg/zon.svg" alt="zon">
                <figure>
                    <img src="{'ImagePlus' | snippet : ['value' => $block.image, 'options' => 'w=1920&zc=1']}" alt="{$block.title}">
                </figure>
            </div>
        </div>
    </div>
</section>