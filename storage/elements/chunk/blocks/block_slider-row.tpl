<a {if $slide.link != ''}href="{$slide.link|url}" title="{$slide.title}" class="has-link" {/if}>
	<div class="wrapper {$count == 'yes' ? 'count' : ''}">
		<div class="{$count == 'yes' ? 'counter' : ''}"></div>
		<div>
			<figure>
				{'ImagePlus' | snippet : ['value' => $slide.image, 'docid' => $id, 'options' => 'w=352&h=198&zc=1&q=80', 'type' => 'tpl']}
			</figure>
			{if $slide.title != ''}<h2 class="heading-s">{$slide.title}</h2>{/if}
			{if $slide.text != ''}<p>{$slide.text}</p>{/if}
		</div>
	</div>
</a>
