<section class="block_slider">
	<div class="grid-container">
		<div class="grid-x align-middle">

			<div class="cell xlarge-7 small-order-1">
				<header>
					<img class="decoration" src="/static/default/media/svg/elements.svg" alt="decoration">
					<h2 class="heading-m">{$block.title}</h2>
					{$block.text}
				</header>
			</div>

			<div class="cta cell xlarge-3 xlarge-offset-2 small-order-3 xlarge-order-2">
				{if $block.link_title != ''}<a class="button secondary white" href="{$block.link_href|url}">{$block.link_title}</a>{/if}
			</div>

			<div class="cell slider {$block.count == 'yes' ? 'count' : ''} small-order-2 xlarge-order-3">

				{set $slides = json_decode($block.slides, true)}
				{foreach $slides as $idx => $slide}
					{include ('file:chunk/blocks/block_slider-row.tpl') length=$slides|length slide=$slide count=$block.count}
				{/foreach}

			</div>

			<div class="progress" role="progressbar"></div>
			<div class="slider-buttons"></div>

		</div>
	</div>
	<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="75.28600311279297 0 339.427978515625 490" xml:space="preserve">
		<g><polygon fill="#3FC3DA" points="75.286,490 414.714,490 245,0"></polygon></g>
	</svg>
</section>