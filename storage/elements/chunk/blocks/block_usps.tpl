<section class="usps">
    <div class="grid-container">
        {if $block.title != ''}
        <div class="grid-x align-center">
            <div class="cell small-12 large-8 text-center">
                <header>
                    <img class="decoration" src="/static/default/media/svg/elements.svg" alt="decoration">
                    <h2 class="heading-m">{$block.title}</h2>
                    {if $block.introtext != ''}<p>{$block.introtext}</p>{/if}
                </header>
            </div>
        </div>
        {/if}
        <div class="usps__cards grid-x align-center text-center">

            {set $usps = json_decode($block.usps, true)}
            {foreach $usps as $idx => $item}
                {if $idx < 3}
                    {include ('file:chunk/blocks/block_usps-row.tpl') length=$usps|length item=$item}
                {/if}
            {/foreach}

        </div>
        {if $block.link_href != ''}<div class="grid-x align-center">
            <a class="button secondary white" href="{$block.link_href|url}">{$block.link_text}</a>
        </div>{/if}
    </div>
    <img class="decoration circle" src="/static/default/media/svg/oval.svg" alt="circle">
</section>