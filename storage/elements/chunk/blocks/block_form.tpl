<section class="block_form">
    <div class="grid-container">
        <div class="form grid-x align-center">
            <div class="cell small-12 large-8 text-center">
                <header>
                    {if $block.title != ""}
                        <h2 class="heading-m">{$block.title}</h2>
                    {/if}
                    {if $block.text != ''}{$block.text}{/if}
                </header>
            </div>

            <div class="form__fields cell small-12 large-5 {$block.title_form == '' ? 'no-title' : ''}">
                {include ('file:chunk/form/' ~ $block.form ~ '.tpl') redirect=$block.redirect}
            </div>


            <div class="form__image cell small-12 large-6 large-offset-1">

                {if $block.image == ''}
                    <img class="form__image-triangle" src="/static/default/media/svg/triangle.svg" alt="triangle">
                    <img class="form__image-oval" src="/static/default/media/svg/oval.svg" alt="oval">
                    <img class="form__image-rectangle" src="/static/default/media/svg/rectangle.svg" alt="rectangle">

                <div id="map"></div>
                {ignore}
                    <script>
                        var map;
                        var marker;
                        var jeugdfonds = {lat: 52.376504, lng: 6.684272};

                        function initMap() {
                            map = new google.maps.Map(document.getElementById('map'), {
                                center: jeugdfonds,
                                zoom:  16
                            });

                            var address =
                                '<div class="infowindow">' +
                                '<h6>Kinderfonds</h6>' +
                                '<p>Het Torentje</p>'+
                                '<p>Hedemanplein 3</p>'+
                                '<p>7603 WJ Almelo</p>'+
                                '<a href="tel:0546888888"></a>'+
                                '</div>';

                            var infowindow = new google.maps.InfoWindow({
                                content: address
                            });

                            marker = new google.maps.Marker({
                                map: map,
                                animation: google.maps.Animation.DROP,
                                position: jeugdfonds
                            });

                            marker.addListener('click', function() {
                                infowindow.open(map, marker);
                            });
                        }
                    </script>

                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBS8IoTzu0iGD71pPwgp2APqFBOX0wAkEg&callback=initMap"async defer></script>
                {/ignore}

                {else}

                    <div class="form__image cell small-12 large-6 large-offset-1">
                        <img class="form__image-triangle" src="/static/default/media/svg/triangle.svg" alt="triangle">
                        <img class="form__image-oval" src="/static/default/media/svg/oval.svg" alt="oval">
                        <img class="form__image-rectangle" src="/static/default/media/svg/rectangle.svg" alt="rectangle">
                        <figure>
                            <img src="{'ImagePlus' | snippet : ['value' => $block.image, 'options' => 'w=1920&zc=1']}" alt="side image">
                        </figure>
                    </div>

                {/if}
            </div>
        </div>
    </div>
    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="75.28600311279297 0 339.427978515625 490" xml:space="preserve">
        <g><polygon fill="#3FC3DA" points="75.286,490 414.714,490 245,0"></polygon></g>
    </svg>
</section>
