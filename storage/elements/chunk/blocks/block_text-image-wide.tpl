<section class="block_text-image-wide">
    <div class="grid-container">
        <div class="grid-x align-middle">
            <div class="text cell large-12 xlarge-5">
                <header>
                    <img class="decoration" src="/static/default/media/svg/elements.svg" alt="decoration">
                    <h2 class="heading-m">{$block.title}</h2>
                </header>
                {$block.text}
                {if $block.link_text != ''}<div><a class="button secondary transparent" href="{$block.link_href|url}">{$block.link_text}</a></div>{/if}
            </div>
            <div class="image cell large-5 large-offset-1">
                <figure>
                    <img src="{'ImagePlus' | snippet : ['value' => $block.image, 'options' => 'w=1920&zc=1']}" alt="{$block.title}">
                </figure>
            </div>
        </div>
    </div>
    <img class="decoration triangle" src="/static/default/media/svg/triangle-long.svg" alt="triangle">
</section>