<section class="block_overview">
    <div class="grid-container">
        {if $block.title != ""}
            <div class="grid-x">
                <div class="cell large-12">
                    <header>
                        <img class="decoration" src="/static/default/media/svg/elements.svg" alt="decoration">
                        <h2 class="heading-m">{$block.title}</h2>
                    </header>
                </div>
            </div>
        {/if}
        <div class="grid-x grid-x grid-margin-x grid-margin-y">

            {'pdoResources' | snippet : [
            'parents'=> $block.parent? : 0,
            'resources'=> $block.resources,
            $resources != '' ? ['sortby' => 'FIELD(modResource.id, '~$block.resources~')'] : '',
            'sortdir'=> 'DESC',
            'showHidden'=> '1',
            'useWeblinkUrl'=> '1',
            'tpl'=> 'block_overview-row',
            'select'=> ['modResource' => 'id,pagetitle,longtitle,introtext,publishedon'],
            'context'=> $_modx->resource.context_key,
            'depth'=> '0',
            'limit'=> '12',
            'includeTVs'=> 'media_overview-image,customer_date,case_tags',
            'processTVs'=> '1'
            'show_image' => $block.show_image,
            'show_introtext' => $block.show_introtext,
            'show_cta' => $block.show_cta,
            'show_quote' => $block.show_quote,
            'columns' => $block.columns,
            'show_title' => $block.show_title,
            'show_date' => $block.show_date,
            'clickable' => $block.clickable
            ]}

        </div>
    </div>
</section>